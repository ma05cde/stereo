function Plot_Spectral_analysis

dir1  = '../FIGURES';

fig1     = figure('position',[50 150 500 400]);
fig1sub1 = subplot('position',[0.20 0.20 0.75 0.75]);

file = '../DATA/Surface_Elevation_WG.mat'; 
load(file)

t = (Mtime_UTC - Mtime_UTC(1)) * 24*3600;

% fft parameter
NF = 1024;

% dimension
Npts = size(Z,2);
Nt   = size(Z,1);

% compute dt, df, f, fs
dt  = median(diff(t));
df  = 1/(dt*NF);
f   = linspace(0,(NF-1)*df, NF);
sht = floor(NF/2);
fs  = circshift(f' ,sht);
fs(1:sht) = fs(1:sht) -f(NF) -df;

% 1D filtering windows
hanningt = 0.5 * (1-cos(2*pi*linspace(0,NF-1,NF)/(NF-1)));

% filtering window correction factors
wc2t = 1/mean(hanningt.^2);

% initialyse Z-pdf
dZ = 0.05;
Zbins = (-2:dZ:2)';
Zhist = zeros(size(Zbins));

% initialyse spectra
Fspec  = zeros([NF,1]); Nf  = 0;

% analysis
N = 2*ceil(Nt/NF)-1;
percent_done = NaN;
for i = 1:N
    
    % limits
    iT_deb = (i-1)*NF/2 +1;
    iT_fin = min(iT_deb+NF-1,Nt);
    Nt_loc = iT_fin-iT_deb+1;
    
    if Nt_loc ~= NF
        continue
    end
    
    % get Z
    T1 = t(iT_deb:iT_fin);
    Z1 = Z(iT_deb:iT_fin,:)';
    
    %
    % Computes 1D frequency spectrum
    %
    
    % loop over points
    for ipt = 1:Npts
        
        % get time serie
        Zt = squeeze(Z1(ipt,:));
        
        % if NaN are found, skip
        if any(isnan(Zt(:)))
            continue
        end
        
        % Detrend in t space
        Zd = detrend(Zt);
        
        % apply window
        Zw = Zd(:) .* hanningt(:);
        
        % computes fft
        Zf = circshift(fft(Zw) / NF, sht);
        
        % normalizes and corrects for window
        Fspec = Fspec + abs(Zf).^2 * wc2t / df;
        
        if any(isnan(Fspec(:)))
            keyboard
        end
        
        % spectra counter
        Nf = Nf+1;
        
    end
end

% compute average
Fspec  = Fspec  / Nf;

% frequency spectra
mask = fs > 0;
freq  = squeeze(fs(mask));
Fspec = squeeze(2*Fspec(mask));

figure(fig1); subplot(fig1sub1)
plot(freq,Fspec)
set(gca,'Xscale','log','Yscale','log')
axis([0.0780    6.4863    0.0000    0.6310])
annotation(fig1,'ellipse',[0.23 0.505 0.162 0.3575]);
grid on
set(gca,'fontsize',14)
xlabel('$f$ [Hz]','interpreter','latex','fontsize',16)
ylabel('$E(f)$ [m$^2$/Hz]','interpreter','latex','fontsize',16)


annotation(fig1,'textarrow',[0.458 0.38],[0.8715 0.8],...
    'TextEdgeColor','w',...
    'TextBackgroundColor',[1 1 1],...
    'FontSize',16,...
    'FontName','Bitstream Charter',...
    'String',{'Swell'});

annotation(fig1,'textarrow',[0.718 0.588],[0.775 0.65],...
    'TextEdgeColor','w',...
    'TextBackgroundColor',[1 1 1],...
    'FontSize',16,...
    'FontName','Bitstream Charter',...
    'String',{'Wind sea'});


fig1_name = sprintf('%s/Spectral_analysis.eps',dir1);
set(fig1, 'PaperPositionMode', 'auto');
fprintf('save figure in %s ... ', fig1_name)
print(fig1,'-depsc',fig1_name)
fprintf('DONE\n')

leg1_name = sprintf('%s/Spectral_analysis.tex',dir1);
fprintf('save legend in %s ... ', leg1_name)
fid1 = fopen(leg1_name,'w');

Hsig = 4*sqrt(sum(Fspec*df));

fc = 0.23;
Hsig1 = 4*sqrt(sum(Fspec(freq<=fc)*df));
Hsig2 = 4*sqrt(sum(Fspec(freq>=fc)*df));

fprintf(fid1,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% \n');
fprintf(fid1,'\\begin{figure}[H!] \n');
fprintf(fid1,'\\centerline{\\includegraphics[width=0.7\\textwidth]{Spectral_Analysis.eps}} \n');
fprintf(fid1,'\\caption{Frequency analysis of a sea surface elevation time series obtained from capacitance wave gauge in the Black Sea on 3/10/2013. ');
fprintf(fid1,'The wave field is composed by both variance from the developing wind sea (right part of the spectrum) and from swell wave (left part of the spectrum).\n');
fprintf(fid1,'The global significant wave height is $H_{\\rm sig}=4*\\sqrt{\\int_0^\\infty E(f)\\d f} = %4.2f$. \n', Hsig);
fprintf(fid1,'The significant wave height of swell waves is $H_{\\rm sig, swell}=4*\\sqrt{\\int_0^{0.23} E(f)\\d f} = %4.2f$. \n', Hsig1);
fprintf(fid1,'The significant wave height of wind sea waves is $H_{\\rm sig, swell}=4*\\sqrt{\\int_{0.23}^{\\infty} E(f)\\d f} = %4.2f$.} \n', Hsig2);
fprintf(fid1,'\\label{fig_Spectral_Analysis} \n');
fprintf(fid1,'\\end{figure} \n');
fprintf(fid1,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% \n');

fclose(fid1);
fprintf('DONE\n')

end