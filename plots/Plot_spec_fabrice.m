% plot_spec_fabrice
run_dir= '/home/cercache/project/stereo/CRIMEA_2013/DIATEAM/POSITION2/';
cd(run_dir)

d=dir('run_2*');
addpath('~/Dropbox/PhD/Rotinas/Spectra/')

for i=3:length(d)
    warning off
    if (i==1) || (i==24) || (i==33)
        disp('BAD directory')
    else
        cd([run_dir,d(i).name])
         %if exist('3D/Surfaces_interp_analyse.mat','file')
         %   Plot_3D_spectra('3D/Surfaces_interp_analyse.mat', 'on', 'on')
         %else
         %   disp(['Missin spec file at, ',d(i).name])
         %end
         if exist('3D/Surfaces_interp_smooth_analyse.mat','file')
            spectrum3D_v5('3D/Surfaces_interp.nc')
         else
             disp(['Missing Surface file at, ',d(i).name])
         end
     end
     pause(3);
     close all;
end



%% WASS

% plot_spec_fabrice
run_dir= '/home/cercache/project/stereo/CRIMEA_2013/WASS/';
cd(run_dir)

d=dir('2013*-');
addpath('~/Dropbox/PhD/Rotinas/Spectra/')

for i=1:length(d)-1 % the last folder is a bad directory
    warning off
    %if (i==1) || (i==24) || (i==33)
    %    disp('BAD directory')
    %else
        cd([run_dir,d(i).name])
        % if exist('3D/Surfaces_interp_analyse.mat','file')
        %    Plot_3D_spectra('3D/Surfaces_interp_analyse.mat', 'on', 'on')
        % else
        %    disp(['Missin spec file at, ',d(i).name])
        % end
         if exist('3D/Surfaces_interp_smooth_analyse.mat','file')
            spectrum3D_v5('3D/Surfaces_interp.nc')
         else
             disp(['Missin Surface file at, ',d(i).name])
         end
     %end
     pause(3); 
     close all;
end
