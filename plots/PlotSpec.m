function [p,cc] = PlotSpec(F,TH,Efth,varargin)
%
% This function plot polar spectrum.
%
%
%
%
%
%

opt1 = 1;
opt2 = 1;

LabelSpacing    = 144;
LabelFontSize   =  11;
LabelColor      =  'w';
LabelFontWeight = 'normal';
Units           = '';
CTick           = [];
CTickLabel      = {};
Interpreter     = 'none';

i = 1;
while i <= length(varargin)
    if strcmpi(varargin{i},'pcolor')
        opt1 = 1;
        i = i+1;
        continue
    elseif strcmpi(varargin{i},'mesh')
        opt1 = 2;
        i = i+1;
        continue
    elseif strcmpi(varargin{i},'CTick')
        i = i+1;
        if ~isvector(varargin{i})
            error('PlotSpec: ''CTick'' must be a vector')
        end
        CTick = varargin{i};
        i = i+1;
    elseif strcmpi(varargin{i},'CTickLabel')
        i = i+1;
        if ~iscell(varargin{i})
            error('PlotSpec: ''CTick'' must be a cell array')
        end
        CTickLabel = varargin{i};
        i = i+1;
    elseif strcmpi(varargin{i},'LabelSpacing')
        i = i+1;
        LabelSpacing = varargin{i};
        i = i+1;
        continue
    elseif strcmpi(varargin{i},'LabelColor')
        i = i+1;
        LabelColor = varargin{i};
        i = i+1;
        continue
    elseif strcmpi(varargin{i},'LabelFontSize')
        i = i+1;
        LabelFontSize = varargin{i};
        i = i+1;
        continue
    elseif strcmpi(varargin{i},'LabelFontWeight')
        i = i+1;
        LabelFontWeight = varargin{i};
        i = i+1;
        continue
    elseif strcmpi(varargin{i},'Units')
        i = i+1;
        Units = varargin{i};
        i = i+1;
        continue
    elseif strcmpi(varargin{i},'Interpreter')
        i = i+1;
        Interpreter = varargin{i};
        i = i+1;
        continue

        
    elseif strcmpi(varargin{i},'nocircle')
        i = i+1;
        CTick      = [];
        CTickLabel = {};
        warning(['PlotSpec: Option ''nocircle will become obselete. \n' ...
            'Please use "''CTick'',[]"'])
        continue
    elseif strcmpi(varargin{i},'circle')
        i = i+1;
        CTick = varargin{i};
        i = i+1;
        warning(['PlotSpec: Option ''circle will become obselete. \n' ...
            'Please use ''CTick'' (and ''CTickLabel'' instead'])
        continue

    
    else
        error('PlotSpec: %s is not a possible argument.\n' , varargin{i})
    end
end

F    = squeeze(F);
TH   = squeeze(TH);
Efth = squeeze(Efth);

Efth(~isfinite(Efth)) = NaN;

if size(Efth,2) ~= length(F) || size(Efth,1) ~= length(TH)
    error('PlotSpec: Dimentions are not agree.')
end

TH(end+1)     = TH(1);
Efth(end+1,:) = Efth(1,:);

[f, th] = meshgrid(F, TH);

[fx,fy] = pol2cart(th,f);

if opt1 == 1
    p=pcolor(fx,fy,Efth);
    shading flat
    axis image
else
    p=mesh(fx,fy,Efth);
end

if ~isempty(CTickLabel) && numel(CTickLabel) ~= numel(CTick)
    error('PlotSpec: ''CTick'' and ''CTickLabel'' must have same dimension')
end

CTick_out = CTick < min(F) | CTick > max(F);
if sum(CTick_out) > 0
    warning('PlotSpec: At least one CTick is out of plot')
    CTick(CTick_out) = [];
    if ~isempty(CTickLabel)
        CTickLabel(CTick_out) = [];
    end
end

if ~isempty(CTick)
    ff = sqrt(fx.^2+fy.^2);
    hold on
    
    for iCTick = 1:numel(CTick)
        
        if CTick(iCTick) < min(F) || CTick(iCTick) > max(F)
            warning('PlotSpec: At least one CTick = %f is out of plot',...
                CTick(iCTick))
            continue
        end
    
        [c{iCTick},h{iCTick}] = contour(fx,fy,ff,CTick(iCTick),'--k');
        cc{iCTick} = clabel(c{iCTick},h{iCTick},...
            'color',LabelColor,...
            'Rotation',0,...
            'LabelSpacing',LabelSpacing,...
            'FontSize',LabelFontSize,...
            'FontWeight',LabelFontWeight,...
            'Interpreter',Interpreter);

        % CTickLabel
        if ~isempty(CTickLabel)
            string = CTickLabel{iCTick};
        else
            string = get(cc(1), 'String');
        end
        
        % Units
        if ~isempty(Units)
            string = [string ' ' Units];
        end
        
        for i = 1:numel(cc{iCTick})
            set(cc{iCTick}(i), 'String', string);
        end
        
    end
    
end

mask = isfinite(Efth);
if ~any(mask)
    error('PlotSpec: Any finite values in spectrum')
end

Emin = min(Efth(mask));
Emax = max(Efth(mask));

if Emax - Emin > 0
    caxis([Emin Emax])
else
    warning('PlotSpec: All values in spectrum are equal to %f !', Emin)
    caxis([Emin-1 Emax+1])
end

end