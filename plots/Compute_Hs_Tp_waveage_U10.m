function [hs,tp,waveage,U10,Udir]=Compute_Hs_Tp_waveage_U10(datei,datef,wind_source,wave_source)
% example:
% datai and dataf are the start and end of the acqusition in datenum, to process the
% components at the same time of the aquisition.
%    datei=datenum([2013 09 25 11 30 00]);
%    datef=datenum([2013 09 25 12 00 00]);
%
%    wind_source='/home/cercache/project/stereo/CRIMEA_2013/Wind/meteo2013.mat';
%    wave_source='3D/Surfaces_interp_analyse.mat';

%--------------------------------------------------------------------------
% Verify source data
%-------------------------------------------------------------------------
% check for wind data
if ~exist(wind_source,'file')
    fprintf('Error %s do not exist \n',wind_source)
    hs=NaN;tp=NaN;waveage=NaN;U10=NaN;
    return
end
% check for wave data
if ~exist(wave_source,'file')
    fprintf('Error %s do not exist \n',wave_source)
    hs=NaN;tp=NaN;waveage=NaN;U10=NaN;
    return
end

%--------------------------------------------------------------------------
% WIND
%--------------------------------------------------------------------------
fprintf('Processing wind date: ...')
WND=load(wind_source,'MTime_UTC','WS','WD');
f=find( (WND.MTime_UTC<=datef) & (WND.MTime_UTC>=datei) );
%wnd_time=WND.MTime_UTC(f);
wnd_dir=WND.WD(f);
wnd_vel=WND.WS(f);
clear WND f
% data is at 23 m high
U10=mean(wnd_vel*log(10/0.0002)/log(23/0.0002));

% define directions categories.
kc1a=length(find(wnd_dir>=0     & wnd_dir< 22.5));
kc2 =length(find(wnd_dir>=22.5  & wnd_dir< 67.5));
kc3 =length(find(wnd_dir>=67.5  & wnd_dir<112.5));
kc4 =length(find(wnd_dir>=112.5 & wnd_dir<157.5));
kc5 =length(find(wnd_dir>=157.5 & wnd_dir<202.5));
kc6 =length(find(wnd_dir>=202.5 & wnd_dir<247.5));
kc7 =length(find(wnd_dir>=247.5 & wnd_dir<292.5));
kc8 =length(find(wnd_dir>=292.5 & wnd_dir<337.5));
kc1b=length(find(wnd_dir>=337.5 & wnd_dir<360.0)); 

kcts=[kc1a+kc1b, kc2, kc3, kc4, kc5, kc6, kc7, kc8];
kcts_label=['N '; 'NE';'E ';'SE';'S ';'SW';'W ';'NW'];

fkc =find(kcts==max(kcts));
Udir=kcts_label(fkc,:);

fprintf('\b\b\b DONE \n')

%--------------------------------------------------------------------------
% WAVE SPECTRUM
%--------------------------------------------------------------------------
fprintf('Processing wave spectrum date: ...')
WAVE=load(wave_source,'Fspec','freq');
hs = 4*sqrt(trapz(WAVE.freq,WAVE.Fspec)) ;
[emax,imax]=max(WAVE.Fspec);
nf = length(WAVE.freq) ;
fp=sum(WAVE.Fspec(max(imax-1,1):min(imax+1,nf)).*WAVE.freq(max(imax-1,1):min(imax+1,nf)))/sum(WAVE.Fspec(max(imax-1,1):min(imax+1,nf)));
tp = 1./fp ;
dpt=30;
waveage = 2*pi*fp/dispNewtonTH(fp,dpt)/U10;
fprintf('\b\b\b DONE \n')



% function [hs,tp,x] = f(e3d,d1,d2,d3,wsp,dpt)
% wsp : wind speed at 10 m
% e3d : 3 dimensional surface elevation spectrum (cartesian, polar)
% d1 : coordinates along the first dimension (vector) -> must be frequency [Hz]
% d2 : coordinates along the second dimension (vector)
% d3 :coordinates along the third dimension (vector)
% dpt : water depth [m]
% hs : wave significant height [m]
% tp : peak period [s]
% x : wave age
% 
% ef = trapz(d2,(trapz(d3,e3d))) ;
% hs = 4*sqrt(trapz(d1,ef)) ;
% [emax,imax]=max(ef);
% nf = length(d1) ;
% fp=sum(ef(max(imax-1,1):min(imax+1,nf)).*d1(max(imax-1,1):min(imax+1,nf)))/sum(ef(max(imax-1,1):min(imax+1,nf)));
% tp = 1./fp ;
% x = 2*pi*fp/dispNewtonTH(fp,dpt)/wsp;



function dispNewtonTH=dispNewtonTH(f,dep)
% inverts the linear dispersion relation (2*pi*f)^2=g*k*tanh(k*dep) to get
% k from f and dep. 2 Arguments: f and dep.
eps=0.000001;
g=9.81;
sig=2.*pi.*f;
Y=dep.*sig.^2./g ;   %a is the squared adimensional frequency
X=sqrt(Y);
I=1;
F=1.;
while abs(max(F)) > eps
H=tanh(X);
F=Y-X.*H;
FD=-H-X./cosh(X).^2;
X=X-F./FD;
end
dispNewtonTH=X./dep;


