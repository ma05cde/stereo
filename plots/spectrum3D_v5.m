function spectrum3D_v5(filename)
%clear all;
%addpath ~/TOOLS/MATLAB/
%filename='/media/iowagaHD1/KATSIVELI/3D_DW_Proj/Surfaces_smooth_for_spectra.nc';
%filename='/media/iowagaHD1/KATSIVELI/3D_DW_Proj/Surfaces_for_spectra.nc';
%filename='3D/Surfaces_interp_smooth.nc';
%filename='3D/Surfaces_interp.nc';
%% Open Surfaces NetCDF file
NCid1=netcdf.open(filename,'NC_NOWRITE');
vartime = netcdf.inqVarID(NCid1,'time');
varx = netcdf.inqVarID(NCid1,'X');
vary = netcdf.inqVarID(NCid1,'Y');
varz = netcdf.inqVarID(NCid1,'Z');

z_scale=netcdf.getAtt(NCid1,varz,'scale_factor');
x_scale=netcdf.getAtt(NCid1,varx,'scale_factor');
x_off=netcdf.getAtt(NCid1,varx,'add_offset');
y_scale=netcdf.getAtt(NCid1,vary,'scale_factor');
y_off=netcdf.getAtt(NCid1,vary,'add_offset');
xo=double(netcdf.getVar(NCid1,varx)).*x_scale+x_off;
yo=double(netcdf.getVar(NCid1,vary)).*y_scale+y_off;
time=netcdf.getVar(NCid1,vartime);
time=(time-time(1));

%Rotate to have Notrh view
% rt=deg2rad(-82.319); % aproximate rotation algle between the cameras an the north
% xl= xo*cos(rt) - yo*sin(rt);
% yl= xo*sin(rt) + yo*cos(rt);
% xo=xl;
% yo=yl;

%% Define fft param
nfft=1024;  % nfft specifies the FFT length that csd uses.
% define number of frames used
if length(time)< nfft
    fprintf('ERRO: number of frames < than 1024 ')
    return
end

nt=floor(length(time)/nfft)*nfft;
if (nt+nfft/2)<length(time)
    nt=nt+nfft/2;
end

zlong=zeros(nt,3);
Etall=zeros(nt,6);

dit=time(2)-time(1);
Fs=1/dit;                       % sampling frequency in Herz

nxo=size(xo,1);
nyo=size(yo,1);

nx=nxo;ny=nyo;ix0=1;iy0=1;
x=xo(ix0:ix0+nx-1);
y=yo(iy0:iy0+ny-1);

nmes=nt; % total number of frames fft                    
                  
df=Fs/nfft;                   % Frequential resolution 
numoverlap = nfft/2 ;         % numoverlap is the number of samples by which the sections over²p.
Nf=nfft/2+1;
Nfo=(Nf-1)/2;

NS1=floor(nmes/nfft);
NS=NS1*2-1;


%% Define a Hamming and Hanning window
hanning=transpose(0.5 * (1-cos(2*pi*linspace(0,nfft-1,nfft)/(nfft-1))));
hammingx=transpose(0.54-0.46.*cos(2*pi*linspace(0,nx-1,nx)/(nx-1)));
hanningx=transpose(0.5 * (1-cos(2*pi*linspace(0,nx-1,nx)/(nx-1))));
hammingy=transpose(0.54-0.46.*cos(2*pi*linspace(0,ny-1,ny)/(ny-1)));
hanningy=transpose(0.5 * (1-cos(2*pi*linspace(0,ny-1,ny)/(ny-1))));
hamming=transpose(0.54-0.46.*cos(2*pi*linspace(0,nfft-1,nfft)/(nfft-1)));
hanningxy=repmat(hanningx,1,ny).*repmat(hanningy,1,nx)';
hammingxy=repmat(hammingx,1,ny).*repmat(hammingy,1,nx)';

f=linspace(0,(nfft-1)*df,nfft);
wc2=1/mean(hanning.^2);                                % window correction factor
wm2=1/mean(hammingx.^2);                               % window correction factor
wc2x=1/mean(hanningx.^2);                              % window correction factor
wc2y=1/mean(hanningy.^2);                              % window correction factor
dx=(x(nx)-x(1))/(nx-1);
dkx=2*pi/(dx*nx);   
dy=(y(ny)-y(1))/(ny-1);
dky=2*pi/(dy*ny);   
kx=linspace(0,(nx-1)*dkx,nx);
ky=linspace(0,(ny-1)*dky,ny);

%% Define f and k space
shx=floor(nx/2);
shy=floor(ny/2);
sht=floor(nfft/2);

fs=circshift(f',sht);
fs(1:sht)=fs(1:sht)-f(nfft)-df;

kxs=circshift(kx',shx);
kxs(1:shx)=kxs(1:shx)-kx(nx)-dkx;

kys=circshift(ky',shy);
kys(1:shy)=kys(1:shy)-ky(ny)-dky;

E=zeros(nx,ny,nfft);
Etime=zeros(nx,ny,nfft,3);

dospec=1;

[xx yy] = meshgrid(x,y);
xv=reshape(xx,nx*ny,1);
yv=reshape(yy,nx*ny,1);
kx2=repmat(kxs,1,ny);
ky2=repmat(kys',nx,1);

theta2=atan2(ky2,kx2);
knorm=sqrt(kx2.^2+ky2.^2);

INDk6=find(knorm > 6); % k filter for take of the harmonics efect in fft
INDk5=find(knorm > 5);
INDk4=find(knorm > 4);

%% Create spectrum NC output file
% NCid=netcdf.create('spectrum3D_v5.nc','NC_WRITE');
% 
% dimid0 = netcdf.defDim(NCid,'FreqDim',nfft);
% dimid1 = netcdf.defDim(NCid,'kxDim',nx);
% dimid2 = netcdf.defDim(NCid,'kyDim',ny);
% var0=netcdf.defVar(NCid,'f','double',[dimid0]);
% var1=netcdf.defVar(NCid,'kx','double',[dimid1]);
% var2=netcdf.defVar(NCid,'ky','double',[ dimid2]);
% netcdf.putAtt(NCid,var2,'units','rad/m');
% netcdf.putAtt(NCid,var1,'units','rad/m');
% netcdf.putAtt(NCid,var0,'units','Hz');
% netcdf.endDef(NCid);
% 
% 
% netcdf.putVar(NCid,var0,f);
% netcdf.putVar(NCid,var1,kx);
% netcdf.putVar(NCid,var2,ky);


%% Performs the full 3D fft 
% Start of main loop on samples
fprintf('Computing fft spectrum3D_V5: ...     ')
for is=1:NS
   fprintf('\b\b\b\b\b\b\b\b\b %02.0f of %02.0f',is,NS)
   if (is <=NS1)    
        i0=10+(is-1)*nfft;
    else
        i0=10+(is-NS1-1)*nfft+nfft/2;
    end
    % load Z surface
    Z=Read_NetCDF(NCid1,'-var', 'Z','-dim','T', [i0+1:i0+nfft], ...
               'X', [ix0:ix0+nx-1],'Y', [iy0:iy0+ny-1]);
    zall=Z.Z;

    if (is <=NS1)    % single point
        zlong(i0+1:i0+nfft,1)=zall(floor(nx/2),floor(ny/2),:); % central point
        zlong(i0+1:i0+nfft,2)=(zall(floor(nx/2+2),floor(ny/2),:)-zall(floor(nx/2-2),floor(ny/2),:))./(dx.*4);
        zlong(i0+1:i0+nfft,3)=(zall(floor(nx/2),floor(ny/2+2),:)-zall(floor(nx/2),floor(ny/2-2),:))./(dy.*4);
    end

    if dospec==1
        for i=1:nx
            zall_st=0.5.*zall(i,:,2:nfft-1)+ ...
                0.25.*(zall(i,:,3:nfft)+zall(i,:,1:nfft-2));
            zall(i,:,2:nfft-1)=zall_st;
        end

        elevation=double(zall(:,:,1:nfft)); %.*z_scale;

         zd=elevation.*0;
    
        % Detrend in x-y space
    
        % defines the edge 
        IND=find(xv==x(1) | xv==x(end) | yv==y(1) | yv==y(end) );
        for j=1:nfft
            z2a=squeeze(elevation(:,:,j));
     
            % detrend, v1
            %z2b=detrend(z2a,'linear');      % We remove the linear trend from each row
            %z2c=(detrend(z2b','linear'))';  % We remove the linear trend from each column

            % detrend, v2
            %zv=reshape(z2a,nx*ny,1);
            %C = planefitg(xv(IND),yv(IND),zv(IND));
            %zp= xx * C(1) + yy *C(2) + C(3);
            %[ mean(abs((z2a(:,1)-zp(:,1)-z2a(:,end)+zp(:,end))))   mean(abs((z2a(:,1)-z2a(:,end))))] 
            %pause
            zd(:,:,j)=(z2a).*hanningxy;
            %zd(:,:,j)=(z2a-zp).*hanningxy;
            % mean(mean(std(zd,0,1)))*4  % computes Hs 
     
        end
        %zspecx=circshift(fft(zd,[],2)./(nx),[0 shx 0]);
        %Ex=abs(zspecx).^2.*(wm2/(dkx));
        %loglog(kxs./(2*pi),2.*squeeze(mean(mean(Ex,3),2).*kxs.^3),'LineWidth',2)
        %set(gca,'FontSize',14);
        %xlabel('{2 \pi/k_x} (m^{-1})');
        %ylabel('{k_x^3 E(k_x)} (no dimensions)');
        %4.*sqrt(mean(mean(sum(Ex,2)*dkx)))
        %
        % Detrend in time
        %

        for i=1:nx
            for j=1:ny
              %za=detrend(squeeze(zd(i,j,:)),'linear');
              %ze(i,j,:)=squeeze(zd(i,j,:)).*hanning;
              ze(i,j,:)=squeeze(zd(i,j,:)).*hamming;
            end
        end

        zf=circshift(fftn(ze)./(nfft*nx*ny),[ shx shy sht]);

        ze=zf.*0;
        zd=ze;

        highpass=0.5*(1+tanh((abs(fs)-0.8)*10));
        for i=1:nx
            for j=1:ny
                %za=detrend(squeeze(zd(i,j,:)),'linear');
                ze(i,j,:)=ifft(circshift(squeeze(zf(i,j,:)).*highpass,[-sht]))./hamming;
                zd(i,j,:)=ifft(circshift(squeeze(zf(i,j,:)),[-sht]))./hamming;
            end
        end


        Et=(abs(ze).^2).*(wc2x*wc2y)/(dkx*dky);
        Et2=(abs(zd).^2).*(wc2x*wc2y)/(dkx*dky);
        if is<NS1;
            i1=1;i2=nfft;
        else
            i1=nfft/4;
            i2=-1; %nfft;
        end
    
        for i=i1:i2
            A=squeeze(Et(:,:,i)); %filter harmonics efect in fft
            Etall(i0+i,1)=sum(sum(A(INDk4))).*dkx*dky;
            Etall(i0+i,2)=sum(sum(A(INDk5))).*dkx*dky;
            Etall(i0+i,3)=sum(sum(A(INDk6))).*dkx*dky;
            A=squeeze(Et2(:,:,i));
            Etall(i0+i,4)=sum(sum(A(INDk4))).*dkx*dky;
            Etall(i0+i,5)=sum(sum(A(INDk5))).*dkx*dky;
            Etall(i0+i,6)=sum(sum(A(INDk6))).*dkx*dky;
        end
        %zf=circshift(fftn(zd)./(nfft*nx*ny),[shx shy sht]);

        E=E+(abs(zf).^2).*(wc2x*wc2y*wc2/(dkx*dky*df));
        if (is < 19)
            Etime(:,:,:,1+floor((is-1)/6))=Etime(:,:,:,1+floor((is-1)/6))+(abs(zf).^2).*(wc2x*wc2y*wc2/(dkx*dky*df))/6;
        end
        %Ef=sum(sum(E,3),2);
        clear zf
    end % end of test on dospec
end
fprintf('\b\b\b\b\b\b\b\b DONE \n')

E=E./NS;

Ekk=squeeze(sum(E(:,:,nfft/2+1:nfft)));

Efth=zeros(nfft/2,36);

%% SAVE specV5.mat
SAVE='on';
if strcmp(SAVE,'on')
    save('3D/specv5.mat','E','Ekk','Efth','kxs','dkx','kys','dky','nx','ny','fs','A','theta2')
end

%% Plots 2 times the double sided spectrum
fprintf('Start Plots: ... ')
% figure
VISIBLE='off';
%VISIBLE='on';
fig1 = figure('units','inches','position',[.5 .5 11 11],...
    'paperunits','in','paperposition',[.5 .5 11 11],'Visible',VISIBLE);

% subplot
fig1subL1   = subplot('position',[0.07 0.23 0.33 0.32]);
fig1subL2   = subplot('position',[0.07 0.65 0.33 0.32]);
sub_pos=[0.50 0.78 0.185 0.185
         0.72 0.78 0.185 0.185
         0.50 0.55 0.185 0.185
         0.72 0.55 0.185 0.185
         0.50 0.30 0.185 0.185
         0.72 0.30 0.185 0.185
         0.50 0.07 0.185 0.185
         0.72 0.07 0.185 0.185];

fig1sub(1) = subplot('position',sub_pos(1,:));
fig1sub(2) = subplot('position',sub_pos(2,:));
fig1sub(3) = subplot('position',sub_pos(3,:));
fig1sub(4) = subplot('position',sub_pos(4,:));
fig1sub(5) = subplot('position',sub_pos(5,:));
fig1sub(6) = subplot('position',sub_pos(6,:));
fig1sub(7) = subplot('position',sub_pos(7,:));
fig1sub(8) = subplot('position',sub_pos(8,:));

H1=figure('Visible',VISIBLE);
Bk=2.*sum(sum(E,3),2).*(kxs.^3);
loglog(kxs./(2*pi),Bk,'k-','LineWidth',2);
hold on;
fac1=92/120;
fac2=152/120;
loglog(kxs./(2*pi),Bk.*fac1,'k--',kxs./(2*pi),Bk.*fac2,'k--');
xlabel('k_x (rad/m)')
ylabel('Energy ( )')

H2=figure('Visible',VISIBLE);
ii=find(abs(kxs) < 13);
jj=find(abs(kys) < 13);
pcolor(kxs(ii)',kys(jj)',squeeze(E(ii,jj,145))');shading flat;h=colorbar;
xlabel('k_x (m^{-1})')
ylabel('k_y (m^{-1})')
ylabel(h,'Energy ( )')

% figure(4)
% pcolor(kxs',kys',squeeze(E(:,:,45))');shading flat;colorbar
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Coupe kx,ky (f=cte)
%
% plot slices
% kmin = 0.5; dk  = 0.5; kmax  = 20;
% fmin = 0.01; df = 0.01; fmax = 2;
% 
% k_plot  = kmin :dk :kmax;
% f_plot  = fmin:df:fmax;
% 
% thmin = 0.0; dth = 2*pi/36; thmax = 2*pi-dth;
% th_plot = thmin:dth:thmax;


sfs=[0.45 0.6 0.75 0.9 1.05 1.2 1.35 1.6];
db =[25   30  35   40   45   50  55   60];
%scrsz = get(groot,'ScreenSize');
%H3=figure('Position',[1 1 scrsz(3) scrsz(4)]);
%set(gca,'FontSize',16);

j=jet; j(1,:)=[1 1 1];

for i=1:length(sfs)
    set(0,'CurrentFigure',fig1); subplot(fig1sub(i));
    %subplot(2,3,i)
    indf=find(fs > sfs(i));
    ifs=indf(1);
    pcolor(kxs'-dkx*0.5,kys'-dky*0.5,10.*log10(squeeze(E(:,:,ifs)))'+db(i));shading flat;
    colormap(j);caxis([-30 0])
    %colormap(jet); colorbar;
    %pcolor(kxs'-dkx*0.5,kys'-dky*0.5,10.*squeeze(E(:,:,ifs))');shading flat;
    kp=(fs(ifs)*2*pi)^2/9.81;
    axis equal
    axis([-kp kp -kp kp]*2)
    hold on
    theta=linspace(0,2*pi,37);
    xp=cos(theta)*kp;
    yp=sin(theta)*kp;
    plot(xp,yp,'w','LineWidth',2)
    title([num2str(sfs(i),'%1.1f'),'Hz, ',num2str(-db(i),'%2.0f'),' dB'])
    %text(-1.5,1.5,['f=',num2str(sfs(i),'%1.1f'),'Hz, ',num2str(-db(i),'%2.0f'),' dB'])
    if i>=length(sfs)-1
        xlabel('{k_x} (m^{-1})');
    end
    if i==1 || i==3 || i==5 || i==7
        ylabel('{k_y} (m^{-1})');
    end
end
%j=[1 1 1; jet; 0 0 0];
colorbar('Position',...
    [0.922394496823666 0.228247162673392 0.0191999999671906 0.498108448790209]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Coupe f,ky
%
iks=floor(nx/2);
set(0,'CurrentFigure',fig1); subplot(fig1subL1);
%H4=figure(35);
set(gca,'FontSize',16);
pcolor(kys,fs,log10(squeeze(E(iks,:,:))'));shading flat;
ylabel('f (Hz)');
xlabel('{k_y} (m^{-1})');
hold on

kp=(fs(:)*2*pi).^2/9.81;
I=find(fs < 0);
kp(I)=-kp(I);
%plot(fs,kp,'w','LineWidth',2)
plot(fs,kp./sqrt(2),'w--','LineWidth',1)

colormap(j)
axis([-10 10 0 1.5])
caxis([-8 2])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Coupe f,kx
%
%H5=figure(45);
set(0,'CurrentFigure',fig1); subplot(fig1subL2);
iks=floor(ny/2);
pcolor(kxs,fs,log10(squeeze(E(:,iks,:))'));shading flat;
cb = colorbar('SouthOutside','position',[0.07 0.09 0.33 0.03]);
set(get(cb,'xlabel'),'string',...
    'log_{10}E(k,f) [m^3/Hz]', ...
    'interpreter','tex','fontsize',12)
%colorbar
ylabel('f (Hz)');
xlabel('{k_x} (m^{-1})');
hold on

kp=(fs(:)*2*pi).^2/9.81;
I=find(fs < 0);
kp(I)=-kp(I);
plot(fs,kp,'w--','LineWidth',2)

colormap(j)
axis([-10 10 0 1.5])
caxis([-8 2])

fprintf('\b\b\b DONE \n ')
%% SAVE FIGS
SAVE='on';
if strcmp(SAVE,'on')
    fprintf('Printing Figures: ...')
    savedir='FIGURES/SPEC/';
    !mkdir -p FIGURES/SPEC/

    %saveas(H1,[savedir,'H1','_V5','.png'],'png')
    %saveas(H1,[savedir,'H1','_V5','.fig'],'fig')

    %saveas(H2,[savedir,'H2','_V5','.png'],'png')
    %saveas(H2,[savedir,'H2','_V5','.fig'],'fig')

    %saveas(H3,[savedir,'H3','_V5','.png'],'png')
    %saveas(H3,[savedir,'H3','_V5','.eps'],'eps')
    %saveas(H3,[savedir,'H3','_V5','.fig'],'fig')

    %saveas(H4,[savedir,'H4','_V5','.png'],'png')
    %saveas(H4,[savedir,'H4','_V5','.fig'],'fig')

    %saveas(H5,[savedir,'H5','_V5','.png'],'png')
    %saveas(H5,[savedir,'H5','_V5','.fig'],'fig')

    saveas(fig1,[savedir,'fig_spec','_V5','.png'],'png')
    print(fig1,'-depsc',[savedir,'fig_spec','_V5','.eps'])
    saveas(fig1,[savedir,'fig_spec','_V5','.fig'],'fig')

    fprintf('DONE \n ')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ????
% freq=f;
% freqds=fs;
% 
% KFspec=E;
% if dospec==1
% save('spectrum_no_smooth_no_time_detrend_fine.m','E','Etime','freq','freqds',...
%     'kx','ky','Nf','nx','ny','kxs','kys','dx','dy','dkx','dky') 
% end
% 
% [Efth,freq,dir,Ef,a1,b1,a2,b2,ratio]=spectra_from_heave_pitch_roll(zlong,Fs,nfft*4,36);
% 
% save Efth_MEM_no_smooth_no_time_detrend_fine_20cm Efth freq dir Ef a1 b1 a2 b2 ratio zlong Etall
% 
% sc=8;iE=6;
% X=zlong(10:nt-40)';
% Y=Etall(10+sc:nt-40+sc,iE)+Etall(10+sc-1:nt-40+sc-1,iE)+Etall(10+sc+1:nt-40+sc+1,iE) ...
%     +Etall(10+sc+2:nt-40+sc+2,iE)+Etall(10+sc-2:nt-40+sc-2,iE);
% X=X(7000:end);
% Y=Y(7000:end);
% 
%    obs_rms=sum(X.^2);
%    obs_mean=mean(X);
%    obs_scat=sqrt(sum((X-obs_mean).^2));
%    mod_mean=mean(Y);
%    mod_scat=sqrt(sum((Y-mod_mean).^2));
%    nrmse=sqrt(sum((X-Y).^2)./obs_rms);
%    corr=sum((X-obs_mean).*(Y-mod_mean))./(obs_scat*mod_scat)    
