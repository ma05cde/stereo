function grid_surface(xyz,P1,Grid_File,dir_3D,idx,FORCE)
%
% This function grid cloud of rotated points given by Rotates.m
%
% USAGE : Grid_Surface('NNNNNN'[,'MMMMMM'[,...]])
%
% => Input  : 3D/Rot_NNNNNN.mat 
%             3D/Rot_MMMMMM.mat
%             ...
%             3D/Grid.mat
%             P1 = importdata('P1cam.txt');
% => Output : 3D/Surfaces_NNNNNN.nc
%             3D/Surfaces_MMMMM.nc
%             ...
%
% 'Grid.mat'   must contain 'xmin','xmax','ymin','ymax' and 'dgrid'. These
%              parameters define the ouptut grid 
%              (see Define_limits_for_grid.m)
%

% function name
func=dbstack;

fprintf('################################################################\n')
fprintf('%s: Function starts \n', func(1).name)
fprintf('################################################################\n')

% NetCDF version of output file
%NetCDF = 3;
NetCDF = 4;

% Read arguments
%[Nstr,StrNums,FORCE] = read_arguments(varargin{:});

% get date
[~,d,e] = fileparts(pwd);
if strcmp(d(1:3),'run')
    % diateam directory
    date = sscanf([d,e],'run_%4d-%2d-%2d_%2dh%2dm%fsZ')';
else
    % wass directory
    date = sscanf(d,'%4d-%2d-%2d_%2d-%2d-%2d')';
end

date=datenum(date);

% % load limits file
% fprintf('Load pixel zone ... ')
% Z = load(fullfile(dir_3D,'Zone.mat'));
% if ~Z.Zone_Rect
%     Z.imin = min(Z.iR3(:));
%     Z.imax = max(Z.iR3(:));
%     Z.jmin = min(Z.jR3(:));
%     Z.jmax = max(Z.jR3(:));
% end
% fprintf('DONE \n')

% load limits file
fprintf('Load %s ... ',Grid_File)
G = load(Grid_File,'xmin','xmax','ymin','ymax','dgrid','fps');
fps = G.fps;
fprintf('DONE \n')
fprintf('=> xmin  = %f \n', G.xmin)
fprintf('=> xmax  = %f \n', G.xmax)
fprintf('=> ymin  = %f \n', G.ymin)
fprintf('=> ymax  = %f \n', G.ymax)
fprintf('=> dgrid = %f \n', G.dgrid)
fprintf('=> fps   = %f \n', G.fps)

fprintf('Creates output grid ... \n')
G.zmin  = -2.0;
G.zmax  =  2.0;

G.xmin = G.dgrid * floor( (G.xmin + G.dgrid/2) ./ G.dgrid - 1 );
G.xmax = G.dgrid * ceil ( (G.xmax - G.dgrid/2) ./ G.dgrid + 1 );
G.ymin = G.dgrid * floor( (G.ymin + G.dgrid/2) ./ G.dgrid - 1 );
G.ymax = G.dgrid * ceil ( (G.ymax - G.dgrid/2) ./ G.dgrid + 1 );

X  = G.xmin:G.dgrid:G.xmax;
Nx = length(X);

Y  = G.ymin:G.dgrid:G.ymax;
Ny = length(Y);

[XX,YY] = meshgrid(X,Y);
fprintf('DONE \n')

fprintf('Computes NetCDF Attributes ... \n')

% define add offset
AOx = mean([min(X),max(X)]);
AOy = mean([min(Y),max(Y)]);
AOz = 0;
AOi = 1300;%mean([Z.imin,Z.imax]);
AOj = 1300;%mean([Z.jmin,Z.jmax]);
AOf = 30000; 

% define scale factor
SFx = max(abs([G.xmin,G.xmax]-AOx)) / 32000; % X  is save with int16
SFy = max(abs([G.ymin,G.ymax]-AOy)) / 32000; % Y  is save with int16
SFz = max(abs([G.zmin,G.zmax]-AOz)) / 32000; % Z  is save with int16
%SFi = max(abs([Z.imin,Z.imax]-AOi)) / 32000; % iR is save with int16
%SFj = max(abs([Z.jmin,Z.jmax]-AOj)) / 32000; % jR is save with int16
SFi = max(abs(AOi)) / 32000; % iR is save with int16
SFj = max(abs(AOj)) / 32000; % jR is save with int16

% initialize error parameters
NERR = 0;
Failed_Process = struct([]);

% define fill value
FVx = netcdf.getConstant('NC_FILL_SHORT');
FVy = netcdf.getConstant('NC_FILL_SHORT');
FVz = netcdf.getConstant('NC_FILL_SHORT');
FVi = netcdf.getConstant('NC_FILL_SHORT');
FVj = netcdf.getConstant('NC_FILL_SHORT');
fprintf('DONE \n')
fprintf('=> X  : SF = %f, OFF = %f, FV = %f \n', SFx, AOx, FVx)
fprintf('=> Y  : SF = %f, OFF = %f, FV = %f \n', SFy, AOy, FVy)
fprintf('=> Z  : SF = %f, OFF = %f, FV = %f \n', SFz, AOz, FVz)
fprintf('=> iR : SF = %f, OFF = %f, FV = %f \n', SFi, AOi, FVi)
fprintf('=> jR : SF = %f, OFF = %f, FV = %f \n', SFj, AOj, FVj)

% loop over groups
for istr = idx;
    
    StrNum = sprintf('%06.0f',istr);
    fprintf('Processing %06.0f : \n', istr)
    
    try
        
        % netcdf file name
        OutFile = sprintf('%s/Surface_%06.0f.nc',     dir_3D, istr);
        TmpFile = sprintf('%s/Surface_%06.0f_tmp.nc', dir_3D, istr);
        
      
        % already done
        if exist(OutFile,'file')  && ~FORCE
            fprintf('==> ALREADY DONE !\n');
            continue
        end
        
        % remove tmp file
        if exist(TmpFile,'file')
            delete(TmpFile)
        end
        
        % open file
        if NetCDF == 3
            fprintf('Create temporary NetCDF3 file ... ');
            ncid = netcdf.create(TmpFile, 'NC_WRITE');
            fprintf('DONE \n')
        else
            fprintf('Create temporary NetCDF4 file ... ');
            ncid = netcdf.create(TmpFile, 'NETCDF4');
            fprintf('DONE \n')
        end
        
        % creates dimensions
        fprintf('Create dimensions ... ')
        Xdimid = netcdf.defDim(ncid, 'X', Nx);
        Ydimid = netcdf.defDim(ncid, 'Y', Ny);
        Tdimid = netcdf.defDim(ncid, 'T', netcdf.getConstant('NC_UNLIMITED'));
        fprintf('DONE \n')
        
        % creates variables
        fprintf('Create variables ... ')
        Fvarid = netcdf.defVar(ncid, 'F','short', Tdimid);
        netcdf.putAtt(ncid,Fvarid,'add_offset',   AOf);
        netcdf.putAtt(ncid,Fvarid,'scale_factor', 1);
        netcdf.putAtt(ncid,Fvarid,'long_name',    'Frame Number');
        
        Tvarid = netcdf.defVar(ncid, 'time','double', Tdimid);
        netcdf.putAtt(ncid,Tvarid,'unit',         'sec');
        netcdf.putAtt(ncid,Tvarid,'long_name',    ['time in seconds since ' datestr(date,31)]);
        
        Xvarid = netcdf.defVar(ncid, 'X','short', Xdimid);
        netcdf.putAtt(ncid,Xvarid,'add_offset',   AOx);
        netcdf.putAtt(ncid,Xvarid,'scale_factor', SFx);
        if NetCDF == 3
            netcdf.putAtt(ncid,Xvarid,'_FillValue', FVx);
        else
            netcdf.defVarFill(ncid,Xvarid,false,    FVx);
        end
        netcdf.putAtt(ncid,Xvarid,'unit',         'm');
        netcdf.putAtt(ncid,Xvarid,'long_name',    'X-axis (corresponding to horizontal axis of images)');
        
        Yvarid = netcdf.defVar(ncid, 'Y','short', Ydimid);
        netcdf.putAtt(ncid,Yvarid,'add_offset',   AOy);
        netcdf.putAtt(ncid,Yvarid,'scale_factor', SFy);
        if NetCDF == 3
            netcdf.putAtt(ncid,Yvarid,'_FillValue', FVy);
        else
            netcdf.defVarFill(ncid,Yvarid,false,    FVy);
        end
        netcdf.putAtt(ncid,Yvarid,'unit','m');
        netcdf.putAtt(ncid,Yvarid,'long_name',    'Y-axis (corresponding to vertical axis of images)');
        
        Zvarid = netcdf.defVar(ncid, 'Z','short', [Xdimid Ydimid Tdimid]);
        netcdf.putAtt(ncid,Zvarid,'add_offset',   AOz);
        netcdf.putAtt(ncid,Zvarid,'scale_factor', SFz);
        if NetCDF == 3
            netcdf.putAtt(ncid,Zvarid,'_FillValue', FVz);
        else
            netcdf.defVarFill(ncid,Zvarid,false,    FVz);
        end
        netcdf.putAtt(ncid,Zvarid,'unit',         'm');
        netcdf.putAtt(ncid,Zvarid,'long_name',    'Surface Elevation (linear interpolation)');
        
        Ivarid = netcdf.defVar(ncid, 'iR','short', [Xdimid Ydimid Tdimid]);
        netcdf.putAtt(ncid,Ivarid,'add_offset',   AOi);
        netcdf.putAtt(ncid,Ivarid,'scale_factor', SFi);
        if NetCDF == 3
            netcdf.putAtt(ncid,Ivarid,'_FillValue', FVi);
        else
            netcdf.defVarFill(ncid,Ivarid,false,    FVi);
        end
        netcdf.putAtt(ncid,Ivarid,'unit',         'pixel');
        netcdf.putAtt(ncid,Ivarid,'long_name',    'Horizontal coordinate of correponding pixel on right image');
        
        Jvarid = netcdf.defVar(ncid, 'jR','short', [Xdimid Ydimid Tdimid]);
        netcdf.putAtt(ncid,Jvarid,'add_offset',   AOj);
        netcdf.putAtt(ncid,Jvarid,'scale_factor', SFj);
        if NetCDF == 3
            netcdf.putAtt(ncid,Jvarid,'_FillValue', FVj);
        else
            netcdf.defVarFill(ncid,Jvarid,false,    FVj);
        end
        netcdf.putAtt(ncid,Jvarid,'unit',         'pixel');
        netcdf.putAtt(ncid,Jvarid,'long_name',    'Vertical coordinate of correponding pixel on right image');
        
        % put Global attribute
        fprintf('Add global attributes ... ')
        Gvarid = netcdf.getConstant('NC_GLOBAL');
        netcdf.putAtt(ncid,Gvarid,'grid_xmin',G.xmin)
        netcdf.putAtt(ncid,Gvarid,'grid_xmax',G.xmax)
        netcdf.putAtt(ncid,Gvarid,'grid_ymin',G.ymin)
        netcdf.putAtt(ncid,Gvarid,'grid_ymax',G.ymax)
        netcdf.putAtt(ncid,Gvarid,'grid_dx',  G.dgrid)
        netcdf.putAtt(ncid,Gvarid,'grid_dy',  G.dgrid)
        netcdf.putAtt(ncid,Gvarid,'Comment',  ...
            sprintf('Created by %s (%s)',func(1).name,date))
        fprintf('DONE \n')
        
        % end of definition
        netcdf.endDef(ncid);
        
        fprintf('Compute and add values for X and Y ...')
        varX = int16((X-AOx) / SFx);
        varY = int16((Y-AOy) / SFy);
        netcdf.putVar(ncid,Xvarid,varX')
        netcdf.putVar(ncid,Yvarid,varY')
        fprintf('DONE \n')
        
        fprintf('Compute and add values for F and time ...')
        %F = str2double(StrNum);
        F = istr;
        varF = int16(F-AOf);
        varT = F/fps;
        netcdf.putVar(ncid,Fvarid,0,1,varF')
        netcdf.putVar(ncid,Tvarid,0,1,varT')
        fprintf('DONE \n')
        
        % file name
        %RotFile = sprintf('%s/Rot_%s.mat', dir_3D, StrNum);
        
        % load cor file
        %fprintf('Load %s ... ', RotFile)
        %load(RotFile,'xyz','pixR')
        %fprintf('DONE \n')
        
        % get values
        x = double(xyz(1,:)');
        y = double(xyz(2,:)');
        z = double(xyz(3,:)');
        
        % Calculat iR and jR
        %P1 = importdata('P1cam.txt');
        Npts = size(xyz,2);
 
        pt2d = P1 * [xyz;ones(1,size(xyz,2))];
        pt2d = pt2d ./ repmat( pt2d(3,:),3,1);
        
        i = double(pt2d(1,:)');
        j = double(pt2d(2,:)');
        
        % mask NaN
        mask = ~isnan(x.*y.*z.*i.*j);
        
        % interpolation for Z
        fprintf('Linear interpolation for Z ... ')
        Fz = TriScatteredInterp(x(mask),y(mask),z(mask),'linear');
        Z = Fz(XX,YY);
        fprintf('DONE \n')
        
      
        % interpolation for iR
        fprintf('Linear interpolation for iR ... ')
        Fi = TriScatteredInterp(x(mask),y(mask),i(mask),'linear');
        iR = Fi(XX,YY);
        fprintf('DONE \n')
        
        % interpolation for jR
        fprintf('Linear interpolation for jR ... ')
        Fj = TriScatteredInterp(x(mask),y(mask),j(mask),'linear');
        jR = Fj(XX,YY);
        fprintf('DONE \n')

        % mask NaN
        %Inan = isnan(Z);
        Inan = isnan(Z) | isnan(iR) | isnan(jR);
        
        % put Z in file
        fprintf('Put Z in NetCDF file ... ')
        varZ = int16((Z-AOz) / SFz);
        varZ(Inan) = FVz;
        if isempty(find(Inan==1));
            varZ = FVz*int16(ones([Ny,Nx]));
        end
        netcdf.putVar(ncid,Zvarid,[0,0,0],[Nx,Ny,1],varZ')
        fprintf('DONE \n')
        
        % put iR in file
        fprintf('Put iR in NetCDF file ... ')
        varI = int16((iR-AOi) / SFi);
        varI(Inan) = FVi;
        if isempty(find(Inan==1));
            varI = FVi*int16(ones([Ny,Nx]));
        end
        netcdf.putVar(ncid,Ivarid,[0,0,0],[Nx,Ny,1],varI')
        fprintf('DONE \n')
        
        % put jR in file
        fprintf('Put jR in NetCDF file ... ')
        varJ = int16((jR-AOj) / SFj);
        varJ(Inan) = FVj;
        if isempty(find(Inan==1));
            varJ = FVj*int16(ones([Ny,Nx]));
        end
        netcdf.putVar(ncid,Jvarid,[0,0,0],[Nx,Ny,1],varJ')
        fprintf('DONE \n')
        
        % put Frame number in file
        fprintf('Put frame number in NetCDF file ... ')
        varF = int16(istr-AOf);
        netcdf.putVar(ncid,Fvarid,[0],[1],varF)
        fprintf('DONE \n')
        
        % close netcdf file
        fprintf('Close NetCDF file ... ')
        netcdf.close(ncid)
        movefile(TmpFile,OutFile)
        fprintf('DONE \n')
        fprintf('=> %s \n', OutFile)
        
    catch me
        
        NERR = NERR+1;
        
        Failed_Process(NERR).StrNum = StrNum;
        Failed_Process(NERR).Report   = getReport(me);
        
        fprintf('PROCESSING FAILS !!!\n==> SKIPPED\n')
        
    end
    
end

if NERR > 0
    
    fprintf('##########################   WARNING  ##########################\n')
    fprintf('%s: Processing fails for %d couple(s) of images.\n', ...
        func(1).name, NERR);

    % list error
    fprintf('List of skipped process : \n');
    for ierr = 1:NERR
        fprintf('%s \n',Failed_Process(ierr).StrNum);
    end
    fprintf('################################################################\n')

    % error output
    ERR = [];
    for ierr = 1:NERR
        ERR = sprintf('%s ==== Processing of %s ==== \n %s \n', ERR, ...
            Failed_Process(ierr).StrNum, Failed_Process(ierr).Report);
    end
    error(ERR)
    
else
    
    fprintf('################################################################\n')
    fprintf('%s: Program ends without errors\n', func(1).name)
    fprintf('################################################################\n')
    
end

 close all

end

