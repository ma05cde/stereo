close all
clear all
clc


set(0,'defaultAxesFontName', 'Times New Roman')
set(0,'defaultTextFontName', 'Times New Roman')
set(0,'DefaultAxesFontSize',16)
set(0,'DefaultTextFontSize',16)
set(0,'defaultfigurecolor',[1 1 1]*1)
scrsz = get(0,'ScreenSize');
%%
%%%%%%%%%%%%%%%%%%%%
% Sumos, SBG data
%%%%%%%%%%%%%%%%%%%%

addpath '/export/home/perso/gmarecha/Bureau/MATLAB/MATLAB/SBG_matlab'
addpath '/export/home/perso/gmarecha/Bureau/PHD/missions/SUMOS_2021/MATLAB'

path_SBG='/mnt/Science/SUMOS/video_data/sbg_imu_ship'
file='sbg_20210226_170000.dat'



cd '/mnt/Science/SUMOS/video_data/sbg_imu_ship'
fdir_save='~/Bureau/PHD/missions/SUMOS_2021/MATLAB/RAW_MAT_SBG_FILES'
IMU = sbg_load_cat(file,fdir_save)

%% Load data measured by the SBG

%  Euler angles accuracy
theta_acc=IMU.EKF_QUAT.PITCH_ACC;
phi_acc=IMU.EKF_QUAT.ROLL_ACC;
psi_acc=IMU.EKF_QUAT.YAW_ACC;

% Quaternions
q0=IMU.EKF_QUAT.Q0;
q1=IMU.EKF_QUAT.Q1;
q2=IMU.EKF_QUAT.Q2;
q3=IMU.EKF_QUAT.Q3;

Lon=IMU.EKF_NAV.LONGITUDE;
Lat=IMU.EKF_NAV.LATITUDE;
Alt=IMU.EKF_NAV.ALTITUDE;

figure(1000),clf
plot(Lon,Lat)
xlabel('Lon [degE]')
ylabel('Lat [degN]')
%the displacement in meter is:

figure(1001),clf;plot((Lon-mean(Lon))*1852*60*cosd(46),(Lat-mean(Lat))*1852*60,'b.-');
xlabel('X[m]')
ylabel('Y[m]')


%% DCM p.31 SBG manual -> With Euler angles and quaternions
% DCM is the composition of 3 rotations matrix


%Euler angle from quaternion
theta=-asin(2*q1.*q3-2*q0.*q2);%Pitch
phi=atan2((2*q2.*q3+2*q0.*q1),((2*q0.^2+2*q3.^2-1)));%Roll

psi=atan2((2*q1.*q2+2*q0.*q3),(2*q0.^2+2*q1.^2-1));%Yaw

DCM_EULER=[cos(theta).*cos(psi),sin(phi).*sin(theta).*cos(psi)-cos(phi).*sin(psi),cos(phi).*sin(theta).*cos(psi)+sin(phi).*sin(psi);...

cos(theta).*sin(psi),sin(phi).*sin(theta).*sin(psi)+cos(phi).*cos(psi),cos(phi).*sin(theta).*sin(psi)-sin(phi).*cos(psi);...

-sin(theta),sin(phi).*cos(theta),cos(phi).*cos(theta)];

DCM_EUL_RESHAPE=reshape(DCM_EULER,[3,3,size(q0,1)]);

% DCM p.33 SBG manual -> With quaternions



DCM_Q=[2*q0.^2+2*q1.^2-1, 2*q1.*q2-2.*q0.*q3,2*q0.*q2+2*q1.*q3; ...
    2*q1.*q2+2*q0.*q3, 2*q0.^2+2*q2.^2-1,2*q2.*q3-2*q0.*q1; ...
    2.*q1.*q3-2.*q0.*q2,2.*q2.*q3+2.*q0.*q1, 2.*q0.^2+2.*q3.^2-1];

DCM_RESHAPE=reshape(DCM_Q,[3,3,size(q0,1)]);



figure(2),clf

imagesc(abs(DCM_RESHAPE(:,:,10)-DCM_EUL_RESHAPE(:,:,10)))%the difference is not zero, we should work on quaternion data L.Marié
title('R_{Euler}-R_{Quaternion}','fontsize',18)
caxis([-1e-6,1e-6])
colorbar

%% 

% vec(LAb)=LAx vec(x) +LAy vec(y)+LAz vec(z) from the zero SBG

LAx=-792.15E-3; %in [m]
LAy=-125.5E-3; %in [m]
LAz=-213.5E-3; %in [m]

%DCM=[DNx DEx DDx
%     DNy DEy DDy
%     DNz DEz DDz]

DNx=DCM_Q(1,1);
DEx=DCM_Q(1,2);
DDx=DCM_Q(1,3);

DNy=DCM_Q(2,1);
DEy=DCM_Q(2,2);
DDy=DCM_Q(2,3);

DNz=DCM_Q(3,1);
DEz=DCM_Q(3,2);
DDz=DCM_Q(3,3);


%The "distance" ( bras de levier en français) in the geo frame is 
Lx_geo=[LAx*DNx+LAy*DNy+LAz*DNz];
Ly_geo=[LAx*DEx+LAy*DEy+LAz*DEz];
Lz_geo=[LAx*DDx+LAy*DDy+LAz*DDz];

%% Loop over SBG index (in prevision of image rotation)
DCM_EULER=[];
DCM_Q=[];
LX=[];
for i=1:999
    
 DCM_Q=[2*q0(i)^2+2*q1(i)^2-1, 2*q1(i)*q2(i)-2*q0(i)*q3(i),2*q0(i)*q2(i)+2*q1(i)*q3(i); ...
    2*q1(i)*q2(i)+2*q0(i)*q3(i), 2*q0(i)^2+2*q2(i)^2-1,2*q2(i)*q3(i)-2*q0(i)*q1(i); ...
    2.*q1(i)*q3(i)-2*q0(i).*q2(i),2*q2(i).*q3(i)+2*q0(i).*q1(i), 2*q0(i)^2+2*q3(i)^2-1];

    
    phi=atan2((2*q2(i)*q3(i)+2*q0(i)*q1(i)),((2*q0(i)^2+2*q3(i)^2-1)));%Roll, use atan2 instead of arctan 
    %because atan return result between -pi/2,pi/2
    theta=-asin(2*q1(i)*q3(i)-2*q0(i)*q2(i));%Pitch
    psi=atan2((2*q1(i)*q2(i)+2*q0(i)*q3(i)),(2*q0(i)^2+2*q1(i)^2-1));%Yaw
    
    
    
    
    DCM_EULER=[cos(theta)*cos(psi),sin(phi)*sin(theta)*cos(psi)-cos(phi)*sin(psi),cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi);...

cos(theta)*sin(psi),sin(phi)*sin(theta)*sin(psi)+cos(phi)*cos(psi),cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi);...

-sin(theta),sin(phi)*cos(theta),cos(phi)*cos(theta)];

if DCM_EULER-DCM_Q<10^-6
    disp('##########################################################')
    disp('##DCM from EULER angle and DCM from quaternion are equal##')
    disp('##########################################################')
    
   
        % Vessel Heading

      
    
    LAx=-792.15E-3; %in [m]
    LAy=-125.5E-3; %in [m]
    LAz=-213.5E-3; %in [m]

    %DCM=[DNx DEx DDx
    %     DNy DEy DDy
    %     DNz DEz DDz]

    DNx=DCM_Q(1,1);
    DEx=DCM_Q(1,2);
    DDx=DCM_Q(1,3);

    DNy=DCM_Q(2,1);
    DEy=DCM_Q(2,2);
    DDy=DCM_Q(2,3);

    DNz=DCM_Q(3,1);
    DEz=DCM_Q(3,2);
    DDz=DCM_Q(3,3);


    %The "distance" ( bras de levier en français) in the geo frame is 
    Lx_geo=[LAx*DNx+LAy*DNy+LAz*DNz];
    Ly_geo=[LAx*DEx+LAy*DEy+LAz*DEz];
    Lz_geo=[LAx*DDx+LAy*DDy+LAz*DDz];

    LX=[LX Lx_geo];


end
end

%Vessel Heading
%Note that the time is not the same for quaternion and heading data (6 quat. data for 1 heading data)
TIME_STAMP_QUAT=IMU.EKF_QUAT.TIME_STAMP;
HEAD=IMU.GPS1_HDT.GPS_TRUE_HEADING;
TIMESTAMP_GPS=IMU.GPS1_HDT.TIME_STAMP;
figure(10000),clf
plot(TIMESTAMP_GPS,HEAD,'-b')
title('Atalante Heading')


%t_gps_on_imu=interp1(TIMESTAMP_GPS,TIMESTAMP_GPS, TIME_STAMP_QUAT, 'nearest'); %t1 value and look through 
                                                                                %t2 to find the nearest t2 value to the t1 value.
HEADING_INTERP = interp1(TIMESTAMP_GPS,HEAD,TIME_STAMP_QUAT,'linear');
                                                                                
                                                                                
figure(10001),clf 
plot(TIMESTAMP_GPS,HEAD,'-xb')
hold on 
plot(TIME_STAMP_QUAT,HEADING_INTERP,'xr')

title('ATALANTE HEADING')
legend('Raw data','Interpolated data')

%% Looking for a purely zonal displacement
v=IMU.EKF_NAV.VELOCITY_N;
min_v=v(find(v==min(abs(v))));

%% Create a synthetic wave field

%%% Define the area where the surface elevation is reconstructed
nx=70;
x=linspace(0,1000,nx);
y2=repmat(x',1,nx);
x2=repmat(x,nx,1);
y=x;


nth=72;
x1=0:(360/nth):360-360/nth; 
direc=x1'.*pi/180;

%%% Changes dir from nautical (0 is North, clockwise) to trigonometric convention
dirmemo=direc;
direc=pi/2.-direc;
nf=100;%494
freq=linspace(0.05,1.25,nf);


%df=freq.*(1.1-1/1.1)./2; % Frequency spacing (standar modelled frequency)
df=0.0126;
dth=2.*pi./real(nth); % Angular spacing
nt=40;
t=linspace(0,(nt-1)*0.5,nt);


a1_min=0 %ou 0.00049
a1_max=0.96

b1_min=0 % ou 0.0017
b1_max=0.7
alpha=0.008;
g=9.81; % gravity acceleration

alpha=0.008;
Tp=10;
fp=1/Tp;
s=50;%narrow spec for large s
g=9.81; % gravity acceleration
theta_m=90*pi/180;

E_f=alpha*(2*pi).^(-4)*g.^2.*freq.^-5.*exp(-5/4*(freq./fp).^-4); %Pierson, 
SD=cos((dirmemo-theta_m)/2).^(2*s); %Longuet Higgins et al. 1963

E_2D=repmat(E_f,nth,1)';
Efth=ones(size(E_2D));

for i=1:nf
    Efth(i,:)=SD'.*E_2D(i,:);
end

nk=nf;
as=zeros(nk,nth); % Amplitudes [m]
for i=1:nf
    as(i,:)=sqrt(2.*Efth(i,:)*df*dth); % By definition of the spectrum
end




x1=0:(360/nth):360-360/nth;  
nth=size(Efth,2);
nk=nf;
%%% Computes wave number
sig=2*pi.*freq;
ks=sig.^2./g; % from dispersion relation in deep water
phases=rand(nf,nth)*2*pi;
cmap=redblue;
figure(3);clf
for ii=1:nt
%
% initialise la surface a zero
%
zeta=zeros(nx,nx);
hs=0.;
E=0;

for i=1:nk
    for j=1:nth
        zeta(:,:)= zeta(:,:)+as(i,j)*cos(ks(i)*cos(pi/2-direc(j)).*x2 ...
            +ks(i)*sin(pi/2-direc(j)).*y2 + phases(i,j)-sig(i)*t(ii));
    E=E+as(i,j).^2./2.;
    end
end
colormap(cmap)
pcolor(x2,y2,zeta);
xlabel('X[m]')
ylabel('Y[m]')
shading flat
colorbar
caxis([-max(zeta(:)),max(zeta(:))]);

end





%% Rotation between zero cam and zero SBG
phi0=31.8*(pi/180); %(34.4-2.6), measured on board
theta0=0; % both cam and sbg are align to y axis
psi0=(90-1.68)*pi/180; %cam and SBG are  perpendicular - a tiny angle for the SBG (not perfectly aligned on the bar)

[Rx,Ry,Rz]=Rot_MAT(phi0,theta0, psi0);

RTOT=RxSBG_cam.*RySBG_cam.*RzSBG_cam;

LA=[LAx,LAy,LAz];

%camera position in the geographical (NED) frame.
LAx_deg=LAx_geo/(60*1852*cos(lat)); %cos(lat) because delta_lon is not constant over the globe.
LAy_deg=LAy_geo/(60*1852); 

LA_deg=[LAx_deg,LAy_deg,LAz_deg];


%% The left camera is at vec(LAb)=LAx vec(x) +LAy vec(y)+LAz vec(z) from the zero SBG

%%%%%%%%%%%%%%%%%%
%Measurements with rulers/meter
%%%%%%%%%%%%%%%%%%
%LAx=-792.15 mm
%LAy=-125.5 mm
%LAz=-213.5 mm

% So: tcam/sbg=LAx vec(x) +LAy vec(y)+LAz vec(z)

% In the geo. axes the translation between cam and SBG is:

% tcam/sbg=LAx.[Xnorth,Xeast,Xdown]+LAy.[Ynorth,Yeast,Ydown]+LAz.[Znorth,Zeast,Zdown]


% two succesive integrations of accelerometer data ? 
% Basic(i)ally given by the SBG...

%%%%%%%%%%%%%%%%%%
%SBG measurements
%%%%%%%%%%%%%%%%%%
% vec(north),vec(east),vec(down)

% Let's notice that 
%LAnorth=LAx Xnorth+LAy Ynorth+LAz Znorth
%LAeast=LAx Xeast+LAy Yeast+LAz Zeast
%LAdown=LAx Xdown+LAy Ydown+LAz Zdown

%The left camera is, in the geo. system coordinate:

%LONcam=LONsbg+LAeast/(60*1852*cos(lat)) %in deg
%LATcam=LATsbg+LAnorth/(60*1852) %in deg
%ALTcam=ALTsbg-LAdown


%0/ ext calib : Xcam0=Xcam1Rcam0/cam1+tcam0/cam1

%1) Xsbg=XcamRsbg/cam+tcam/sbg

%2) Xgeo=XsbgRgeo/sbg+tcam/geo (in geo.) %DCM is Rgeo/sbg ?


%% Mooving in meter




 