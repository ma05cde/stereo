LACamera=[792.5e-3;125.5e-3;-(104-65.5)*1e-3];

c=cosd(34.44+2.66);s=sind(34.44+2.66);
M=[ 1 0 0;0 c -s;0 s c];

LAInter=M*[LACamera(1);LACamera(2);0];
LACamera_in_SBG=M*LACamera;

figure(1);clf;hold on
plot([0,LACamera(2)],[0,0],'k')
plot([LACamera(2),LACamera(2)],[0,LACamera(3)],'k')
plot([0,LAInter(2)],[0,LAInter(3)],'k')
plot([LAInter(2),LACamera_in_SBG(2)],[LAInter(3),LACamera_in_SBG(3)],'k')
plot(LACamera(2),LACamera(3),'ro');
plot(LACamera_in_SBG(2),LACamera_in_SBG(3),'go');
plot(0,0,'ko')
xlim([-200e-3,200e-3])
ylim([-200e-3,200e-3])
xlabel('YSBG (m)')
set(gca,'XDir','reverse')
set(gca,'YDir','reverse')
ylabel('ZSBG (m)')

axis equal
grid('on')

% Deuxi�me rotation: la SBG est tourn�e de 1.7� vers la droite autour de son axe Z.
c=cosd(1.7);s=sind(1.7);
M=[ c,s,0;
   -s,c,0;
    0,0,1];
LACamera_in_SBG=M*LACamera_in_SBG;

   


disp(sprintf('X = %.3f m',LACamera_in_SBG(1)))
disp(sprintf('Y = %.3f m',LACamera_in_SBG(2)))
disp(sprintf('Z = %.3f m',LACamera_in_SBG(3)))