sdir='/home/cercache/project/stereo/DYNATREZ_2016/raw/'

folders=['run_2016-10-17_15h06m30.189sZ'
'run_2016-10-17_16h41m31.544sZ'
'run_2016-10-19_15h48m06.893sZ'
'run_2016-10-19_16h22m14.074sZ'
'run_2016-10-21_09h03m32.672sZ'
'run_2016-10-24_09h23m18.261sZ'
'run_2016-10-24-09h56m39.848sZ']

for i=1:size(folders,1)
  cd([sdir,folders(i,:)])
  disp(['Creating list for ', folders(i,:)])
  d=dir('Images_Raw/*.tif');
  disp(['     Number of files: ',d(1).name(1:6),' to ',d(end).name(1:6)])
  Create_StrNum_List(str2num(d(1).name(1:6)),str2num(d(end).name(1:6)),6)
  disp('     DONE')
  disp(' ')
end
disp('END OF LIST CREATION')
