function acqui_ind = extract_sbg_index_from_tsfix(sbgdat,stereo_date)


% sbgdat : input : sbg binary data
%          /mnt/Science/SUMOS/video_data/sbg_imu_ship/sbg_20210217_070000.dat
% stereo_date : input : estimated start date of stereo acquisition
%           '20210217_075100_900';
% acqui_ind : output : closest index of sbg when a new trigger start on
%                      event b


% EVENT B : stereovideo trigger signal
% EVENT_B.time : internal time stamp (reset to 0 if SBG is restarted)

%sbgdat='/mnt/Science/SUMOS/video_data/sbg_imu_ship/sbg_20210217_070000.dat';
%stereo_date='20210217_075100_900';



% load IMU data in a structure
IMU=sbg_load_fast(sbgdat);

% compute fixed time stamp 
imu=sbg_imu_time_fields(IMU);


% compute the median frame rate (15fps = 66 666nanosec)
%dt = nanmedian(diff(IMU.EVENT_B.TIME_STAMP));

size(imu.EVENT_B.TS_fix)
plot(diff(imu.EVENT_B.TS_fix),'xr');

% find the indexes where the timestamp has a jump
% of at least 66666us which is related to a new trigger start
eventb_ind=find(diff(imu.EVENT_B.TS_fix)>70000);

% get the timestamp values for all the requested eventb indexes
eventb_start=imu.EVENT_B.TS_fix(eventb_ind);


for i = 1:size(eventb_start)
  % find the indexes where the UTC_TIME closest to eventb timestamps
  utc_ind(i)=find(abs(eventb_start(i)-imu.UTC_TIME.TS_fix)==min(abs(eventb_start(i)-imu.UTC_TIME.TS_fix)));
  datestr(imu.UTC_TIME.time(utc_ind(i)),'yyyymmdd_HHMMSS_FFF');
end


% requested start time
stereo_time=datenum(stereo_date,'yyyymmdd_HHMMSS_FFF');

% find the indexes where the UTC_TIME closest to requested time
stereo_ind = find(abs(stereo_time-imu.UTC_TIME.time) ==...
          min(abs(stereo_time-imu.UTC_TIME.time)));
datestr(imu.UTC_TIME.time(stereo_ind),'yyyymmdd_HHMMSS_FFF');


% find the index where stereo_ind is the closest to utc_ind
acqui_ind=utc_ind(find(abs(stereo_ind-utc_ind)==min(abs(stereo_ind-utc_ind))));
datestr(imu.UTC_TIME.time(acqui_ind),'yyyymmdd_HHMMSS_FFF')


