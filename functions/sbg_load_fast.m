function IMU = sbg_load_fast(fname_sbg,save_mat,fname_out)
% sbg_load.m - script to convert SBG IMU data from raw stream to matlab
% format
%
% Details found in Ellipse Firmware Manual, starting page 46.
%
% **** This file replaces sbg_load.m ****
% --> direct all future development here (2015/10/28)
%
% USAGE:
%   IMU = sbg_load_fast(fname_sbg,save_mat,fname_out)
%
% WHERE:
%   fname_sbg = is the file name of the raw .DAT file to be converted
%               (e.g. 'c:/DATA/031312I1.DAT')
%   save_mat = logical 1= save .mat file, 0=don't
%   fname_out = file name of .mat file.  If blank, this defaults to the
%               same location/name as the input but with a .mat extension.
%
%   IMU = a structure containing all the data from the data file.  Outputs
%         are described in the Ellipse Firmware Manual, starting page 46
%

% Peter Sutherland - Ifremer - 2015/10/07
% PS - 2015/12/15 - CRC check added
% PS - 2016/02/16 - Fixed switched Lat and lon in EKF_NAV fields
% PS - 2018/12/04 - Added PRESSURE packet
% PS - 2021-02-12 - Fixed missing CRC in GPS1_HDT packet

%%
%clear;
mname = 'sbg_load_fast.m';
mdir = 'C:\Users\sutherlandp\Documents\matlab_custom\permanent\instruments';

if nargin<2, save_mat = 0; end

if save_mat
    ifne = max(strfind(fname_sbg,'.')-1);
    if nargin<3
        fname_out = [fname_sbg(1:ifne),'.mat'];
    else
        ifns = max([strfind(fname_sbg,'/'),strfind(fname_sbg,'\')])+1;
        if isempty(ifns), ifns = 1; end
        fname_out = [fdir_save,'/',fname_sbg(ifns:ifne),'.mat'];
    end
end

% if ~(exist(fname_out,'file')==2)




% load it all once to find indices of individual packets
fid = fopen(fname_sbg);
ddec = fread(fid,inf,'uint8','ieee-le');
fclose(fid);

n_dd = length(ddec);

% indices of start of each data packet
ii_index = strfind(ddec',[255 90 ]);
if n_dd < (ii_index(end) + 10)
    ii_index(end) = [];
    disp('Clipping last packet.');
end

%ii_index = ii_index(1:end-1);
MSG = ddec(ii_index + 2);
% CLASS = ddec(ii_index + 3);
% LEN = dconv_u8_u16([ddec(ii_index + 4) ddec(ii_index + 5) ]);


%% Load data into structures
IMU.mname = mname;
IMU.mdir = mdir;
IMU.file_name = fname_sbg;

%% STATUS
ok_m = MSG==1;
N_m = sum(ok_m);

if N_m<2
    disp('No data in field --> STATUS');
else
    disp('Adding field --> STATUS');
    l_m = 22 + 5 + 3; % nanmedian(LEN(ok_m));
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    
    IMU.STATUS.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3)));
    IMU.STATUS.GENERAL_STATUS = dconv_u8_u16(d_m(:,7 + (4:5)));
    IMU.STATUS.RESERVED_0 = dconv_u8_u16(d_m(:,7 + (6:7)));
    IMU.STATUS.COM_STATUS = dconv_u8_u32(d_m(:,7 + (8:11)));
    IMU.STATUS.AIDING_STATUS = dconv_u8_u32(d_m(:,7 + (12:15)));
    IMU.STATUS.RESERVED_1 = dconv_u8_u32(d_m(:,7 + (16:19)));
    IMU.STATUS.RESERVED = dconv_u8_u16(d_m(:,7 + (20:21)));
    IMU.STATUS.CRC =      dconv_u8_u16(d_m(:,7 + (22:23)));
    % IMU.STATUS.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.STATUS.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));

    
    
    
end

%% UTC_TIME
clear N_m l_m d_m;
ok_m = MSG==2;
N_m = sum(ok_m);

if N_m==0
    disp('No data in field --> UTC_TIME');
else
    disp('Adding field --> UTC_TIME');
    l_m = 21 + 5 + 3; % nanmedian(LEN(ok_m));
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    
    IMU.UTC_TIME.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3)));
    IMU.UTC_TIME.CLOCK_STATUS = dconv_u8_u16(d_m(:,7 + (4:5)));
    IMU.UTC_TIME.YEAR = double(dconv_u8_u16(d_m(:,7 + (6:7))));
    IMU.UTC_TIME.MONTH  = double(d_m(:,7 + (8)));
    IMU.UTC_TIME.DAY    = double(d_m(:,7 + (9)));
    IMU.UTC_TIME.HOUR   = double(d_m(:,7 + (10)));
    IMU.UTC_TIME.MIN    = double(d_m(:,7 + (11)));
    IMU.UTC_TIME.SEC    = double(d_m(:,7 + (12)));
    IMU.UTC_TIME.NANOSEC = dconv_u8_u32(d_m(:,7 + (13:16)));
    IMU.UTC_TIME.GPS_TOW = dconv_u8_u32(d_m(:,7 + (17:20)));
    IMU.UTC_TIME.time = datenum(IMU.UTC_TIME.YEAR, IMU.UTC_TIME.MONTH,IMU.UTC_TIME.DAY ,...
        IMU.UTC_TIME.HOUR, IMU.UTC_TIME.MIN, (IMU.UTC_TIME.SEC + IMU.UTC_TIME.NANOSEC/1E+9));
    IMU.UTC_TIME.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.UTC_TIME.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));

end

%% IMU_DATA
clear N_m l_m d_m;

ok_m = MSG==3;
N_m = sum(ok_m);
if N_m==0
    disp('No data in field --> IMU_DATA');
else
    disp('Adding field --> IMU_DATA');
    l_m = 58 + 5 + 3; % nanmedian(LEN(ok_m));
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    
    IMU.IMU_DATA.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3)));
    IMU.IMU_DATA.IMU_STATUS = dconv_u8_u16(d_m(:,7 + (4:5)));
    IMU.IMU_DATA.ACCEL_X = dconv_u8_float32(d_m(:,7 + (6:9)));
    IMU.IMU_DATA.ACCEL_Y = dconv_u8_float32(d_m(:,7 + (10:13)));
    IMU.IMU_DATA.ACCEL_Z = dconv_u8_float32(d_m(:,7 + (14:17)));
    IMU.IMU_DATA.GYRO_X = dconv_u8_float32(d_m(:,7 + (18:21)));
    IMU.IMU_DATA.GYRO_Y = dconv_u8_float32(d_m(:,7 + (22:25)));
    IMU.IMU_DATA.GYRO_Z = dconv_u8_float32(d_m(:,7 + (26:29)));
    IMU.IMU_DATA.TEMP = dconv_u8_float32(d_m(:,7 + (30:33)));
    IMU.IMU_DATA.DATA_VEL_X = dconv_u8_float32(d_m(:,7 + (34:37)));
    IMU.IMU_DATA.DATA_VEL_Y = dconv_u8_float32(d_m(:,7 + (38:41)));
    IMU.IMU_DATA.DATA_VEL_Z = dconv_u8_float32(d_m(:,7 + (42:45)));
    IMU.IMU_DATA.DELTA_ANGLE_X = dconv_u8_float32(d_m(:,7 + (46:49)));
    IMU.IMU_DATA.DELTA_ANGLE_Y = dconv_u8_float32(d_m(:,7 + (50:53)));
    IMU.IMU_DATA.DELTA_ANGLE_Z = dconv_u8_float32(d_m(:,7 + (54:57)));
    IMU.IMU_DATA.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.IMU_DATA.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));
        
end

%% MAG % page 54

clear N_m l_m d_m;

ok_m = MSG==4;
N_m = sum(ok_m);
if N_m==0
    disp('No data in field --> MAG');
else
    disp('Adding field --> MAG');
    l_m = 30 + 5 + 3; % nanmedian(LEN(ok_m));
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    
    IMU.MAG.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3)));
    IMU.MAG.MAG_STATUS = dconv_u8_u16(d_m(:,7 + (4:5)));
    IMU.MAG.MAG_X = dconv_u8_float32(d_m(:,7 + (6:9)));
    IMU.MAG.MAG_Y = dconv_u8_float32(d_m(:,7 + (10:13)));
    IMU.MAG.MAG_Z = dconv_u8_float32(d_m(:,7 + (14:17)));
    IMU.MAG.ACCEL_X = dconv_u8_float32(d_m(:,7 + (18:21)));
    IMU.MAG.ACCEL_Y = dconv_u8_float32(d_m(:,7 + (22:25)));
    IMU.MAG.ACCEL_Z = dconv_u8_float32(d_m(:,7 + (26:29)));
    IMU.MAG.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.MAG.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));

end

%% MAG_CALIB % page 55 (not checked)

clear N_m l_m d_m;

ok_m = MSG==5;
N_m = sum(ok_m);
if N_m==0
    disp('No data in field --> MAG_CALIB');
else
    disp('Adding field --> MAG_CALIB');
    l_m = 22 + 5 + 3; % nanmedian(LEN(ok_m));
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    if size(d_m,2)==1, d_m = d_m'; end
    
    IMU.MAG_CALIB.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3)));
    IMU.MAG_CALIB.RESERVED = dconv_u8_u16(d_m(:,7 + (4:5)));
    IMU.MAG_CALIB.BUFFER = uint8(d_m(:,7 + (6:21)));
    IMU.MAG_CALIB.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.MAG_CALIB.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));

end

%% EKF_EULER % page 52 (not checked)

clear N_m l_m d_m;
ok_m = MSG==6;
N_m = sum(ok_m);
if N_m==0
    disp('No data in field --> EKF_EULER');
else
    disp('Adding field --> EKF_EULER');
    l_m = 32 + 5 + 3; % nanmedian(LEN(ok_m));
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    if size(d_m,2)==1, d_m = d_m'; end
    
    IMU.EKF_EULER.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3)));
    IMU.EKF_EULER.ROLL = dconv_u8_float32(d_m(:,7 + (4:7)));
    IMU.EKF_EULER.PITCH = dconv_u8_float32(d_m(:,7 + (8:11)));
    IMU.EKF_EULER.YAW = dconv_u8_float32(d_m(:,7 + (12:15)));
    IMU.EKF_EULER.ROLL_ACC = dconv_u8_float32(d_m(:,7 + (16:19)));
    IMU.EKF_EULER.PITCH_ACC = dconv_u8_float32(d_m(:,7 + (20:23)));
    IMU.EKF_EULER.YAW_ACC = dconv_u8_float32(d_m(:,7 + (24:27)));
    IMU.EKF_EULER.ROLL = dconv_u8_u32(d_m(:,7 + (28:31)));
    IMU.EKF_EULER.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.EKF_EULER.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));

end

%% EKF_QUAT % page 52

clear N_m l_m d_m;


ok_m = MSG==7;
N_m = sum(ok_m);
if N_m==0
    disp('No data in field --> EKF_QUAT');
else
    disp('Adding field --> EKF_QUAT');
    l_m = 36 + 5 + 3; % nanmedian(LEN(ok_m));
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    if size(d_m,2)==1, d_m = d_m'; end
    
    IMU.EKF_QUAT.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3)));
    IMU.EKF_QUAT.Q0 = dconv_u8_float32(d_m(:,7 + (4:7)));
    IMU.EKF_QUAT.Q1 = dconv_u8_float32(d_m(:,7 + (8:11)));
    IMU.EKF_QUAT.Q2 = dconv_u8_float32(d_m(:,7 + (12:15)));
    IMU.EKF_QUAT.Q3 = dconv_u8_float32(d_m(:,7 + (16:19)));
    IMU.EKF_QUAT.ROLL_ACC = dconv_u8_float32(d_m(:,7 + (20:23)));
    IMU.EKF_QUAT.PITCH_ACC = dconv_u8_float32(d_m(:,7 + (24:27)));
    IMU.EKF_QUAT.YAW_ACC = dconv_u8_u32(d_m(:,7 + (28:31)));
    IMU.EKF_QUAT.SOLUTION_STATUS = bitshift(bitshift(dconv_u8_u32(d_m(:,7 + (32:35))),31-11,'uint32'),11-31,'uint32');  % SOLUTION_STATUS_raw = dconv_u8_u32(d_m(:,7 + (32:35)));
    IMU.EKF_QUAT.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.EKF_QUAT.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));

end

%% EKF_NAV, page 53
clear N_m l_m d_m;

ok_m = MSG==8;
N_m = sum(ok_m);
if N_m==0
    disp('No data in field --> EKF_NAV');
else
    disp('Adding field --> EKF_NAV');
    l_m = 72 + 5 + 3; % nanmedian(LEN(ok_m));
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    
    if size(d_m,2)==1, d_m = d_m'; end
    
    IMU.EKF_NAV.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3)));
    IMU.EKF_NAV.VELOCITY_N = dconv_u8_float32(d_m(:,7 + (4:7)));
    IMU.EKF_NAV.VELOCITY_E = dconv_u8_float32(d_m(:,7 + (8:11)));
    IMU.EKF_NAV.VELOCITY_D = dconv_u8_float32(d_m(:,7 + (12:15)));
    IMU.EKF_NAV.VELOCITY_N_ACC = dconv_u8_float32(d_m(:,7 + (16:19)));
    IMU.EKF_NAV.VELOCITY_E_ACC = dconv_u8_float32(d_m(:,7 + (20:23)));
    IMU.EKF_NAV.VELOCITY_D_ACC = dconv_u8_float32(d_m(:,7 + (24:27)));
    IMU.EKF_NAV.LATITUDE = dconv_u8_float64(d_m(:,7 + (28:35)));
    IMU.EKF_NAV.LONGITUDE = dconv_u8_float64(d_m(:,7 + (36:43)));
    IMU.EKF_NAV.ALTITUDE = dconv_u8_float64(d_m(:,7 + (44:51)));
    IMU.EKF_NAV.UNDULATION = dconv_u8_float32(d_m(:,7 + (52:55)));
    IMU.EKF_NAV.LATITUDE_ACC = dconv_u8_float32(d_m(:,7 + (56:59)));
    IMU.EKF_NAV.LONGITUDE_ACC = dconv_u8_float32(d_m(:,7 + (60:63)));
    IMU.EKF_NAV.ALTITUDE_ACC = dconv_u8_float32(d_m(:,7 + (64:67)));
    IMU.EKF_NAV.SOLUTION_STATUS = dconv_u8_u32(d_m(:,7 + (68:71)));
    IMU.EKF_NAV.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.EKF_NAV.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));
    
end

%% GPS1_VEL, page 55
clear N_m l_m d_m;

ok_m = MSG==13;
N_m = sum(ok_m);
if N_m==0
    disp('No data in field --> GPS1_VEL');
else
    disp('Adding field --> GPS1_VEL');
    l_m = 44 + 5 + 3; % nanmedian(LEN(ok_m));
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    if size(d_m,2)==1, d_m = d_m'; end
    
    IMU.GPS1_VEL.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3))); % [us] since power on
    IMU.GPS1_VEL.GPS_VEL_STATUS = dconv_u8_u32(d_m(:,7 + (4:7)));
    IMU.GPS1_VEL.GPS_TOW = dconv_u8_u32(d_m(:,7 + (8:11)));	% [ms] GPS Time of Week
    IMU.GPS1_VEL.VEL_N = dconv_u8_float32(d_m(:,7 + (12:15)));	% [m/s] Velocity in North direction
    IMU.GPS1_VEL.VEL_E = dconv_u8_float32(d_m(:,7 + (16:19)));
    IMU.GPS1_VEL.VEL_D = dconv_u8_float32(d_m(:,7 + (20:23)));
    IMU.GPS1_VEL.VEL_ACC_N = dconv_u8_float32(d_m(:,7 + (24:27)));	% [m/s] 1 sigma accuracy in North direction
    IMU.GPS1_VEL.VEL_ACC_E = dconv_u8_float32(d_m(:,7 + (28:31)));
    IMU.GPS1_VEL.VEL_ACC_D = dconv_u8_float32(d_m(:,7 + (32:35)));
    IMU.GPS1_VEL.COURSE = dconv_u8_float32(d_m(:,7 + (36:39)));	% [deg] True direction of motion over ground (0 to 360�)
    IMU.GPS1_VEL.COURSE_ACC = dconv_u8_float32(d_m(:,7 + (40:43)));	% [deg] 1 sigma course accuracy
    IMU.GPS1_VEL.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.GPS1_VEL.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));

end

%% GPS1_POS, MSG=14, page 56
clear N_m l_m d_m;

ok_m = MSG==14;
N_m = sum(ok_m);
if N_m==0
    disp('No data in field --> GPS1_POS');
else
    disp('Adding field --> GPS1_POS');
    l_m = 57 + 5 + 3;
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    if size(d_m,2)==1, d_m = d_m'; end
    
    IMU.GPS1_POS.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3)));    % [us] since power on
    IMU.GPS1_POS.GPS_POS_STATUS = dconv_u8_u32(d_m(:,7 + (4:7)));
    IMU.GPS1_POS.GPS_TOW = dconv_u8_u32(d_m(:,7 + (8:11)));       % [ms]
    IMU.GPS1_POS.LAT = dconv_u8_float64(d_m(:,7 + (12:19)));           % [deg]
    IMU.GPS1_POS.LONG = dconv_u8_float64(d_m(:,7 + (20:27)));          % [deg]
    IMU.GPS1_POS.ALT = dconv_u8_float64(d_m(:,7 + (28:35)));           % [m] above sea level
    IMU.GPS1_POS.UNDULATION = dconv_u8_float32(d_m(:,7 + (36:39)));     % [m] diff from geoid
    IMU.GPS1_POS.POS_ACC_LAT = dconv_u8_float32(d_m(:,7 + (40:43)));    % [m]
    IMU.GPS1_POS.POS_ACC_LONG = dconv_u8_float32(d_m(:,7 + (44:47)));   % [m]
    IMU.GPS1_POS.POS_ACC_ALT = dconv_u8_float32(d_m(:,7 + (48:51)));    % [m]
    IMU.GPS1_POS.NUM_SV_USED = double(uint8(d_m(:,7 + (52))));
    IMU.GPS1_POS.BASE_STATION_ID =  dconv_u8_u16(d_m(:,7 + (54:55)));
    IMU.GPS1_POS.DIFF_AGE =  dconv_u8_u16(d_m(:,7 + (56:57)));      % [0.01s]
    IMU.GPS1_POS.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.GPS1_POS.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));

end

%% GPS1_HDT, MSG=15, page 56 << NOT CHECKED >>
clear N_m l_m d_m;

ok_m = MSG==15;
N_m = sum(ok_m);
if N_m==0
    disp('No data in field --> GPS1_HDT');
else
    disp('Adding field --> GPS1_HDT');
    l_m = 26 + 5 + 3 + 4; % nanmedian(LEN(ok_m)); % This last +4 is not documented in the manual (it's a 3 there)
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    if size(d_m,2)==1, d_m = d_m'; end
    
    IMU.GPS1_HDT.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3)));    % [us] since power on
    IMU.GPS1_HDT.GPS_HDT_STATUS = dconv_u8_u16(d_m(:,7 + (4:5)));
    IMU.GPS1_HDT.GPS_TOW = dconv_u8_u32(d_m(:,7 + (6:9)));       % [ms]
    IMU.GPS1_HDT.GPS_TRUE_HEADING = dconv_u8_float32(d_m(:,7 + (10:13)));
    IMU.GPS1_HDT.GPS_TRUE_HEADING_ACC = dconv_u8_float32(d_m(:,7 + (14:17)));
    IMU.GPS1_HDT.GPS_PITCH = dconv_u8_float32(d_m(:,7 + (18:21)));
    IMU.GPS1_HDT.GPS_PITCH_ACC = dconv_u8_float32(d_m(:,7 + (22:25)));
    IMU.GPS1_HDT.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.GPS1_HDT.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));

end


%% PRESSURE, page 58
clear N_m l_m d_m;

ok_m = MSG==36;
N_m = sum(ok_m);
if N_m==0
    disp('No data in field --> PRESSURE');
else
    disp('Adding field --> PRESSURE');
    l_m = 14 + 5 + 3; % nanmedian(LEN(ok_m));
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    if size(d_m,2)==1, d_m = d_m'; end
    
    IMU.PRESSURE.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3)));       % [us] since power on
    IMU.PRESSURE.ALTIMETER_STATUS = dconv_u8_u16(d_m(:,7 + (4:5))); % Altimeter status
    IMU.PRESSURE.PRESSURE = dconv_u8_float32(d_m(:,7 + (6:9)));     % [Pa] Pressure measured by the sensor
    IMU.PRESSURE.ALTITUDE = dconv_u8_float32(d_m(:,7 + (10:13)));	% [m] Altitude computed from altitude 
    
    IMU.PRESSURE.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.PRESSURE.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));

end

%%
clear N_m l_m d_m;

ok_m = MSG==31;
N_m = sum(ok_m);
if N_m==0
    disp('No data in field --> GPS1_RAW');
else
    disp(['Field --> GPS1_RAW --> ' num2str(N_m) ' samples']);
end
    
%% EVENT_A, MSG=24, page 56 << NOT CHECKED >>
clear N_m l_m d_m;

ok_m = MSG==24;
N_m = sum(ok_m);
if N_m==0
    disp('No data in field --> EVENT_A');
else
    disp('Adding field --> EVENT_A');
    l_m = 14 + 5 + 3; % nanmedian(LEN(ok_m));
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    if size(d_m,2)==1, d_m = d_m'; end
    
    IMU.EVENT_A.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3)));    % [us] since power on
    IMU.EVENT_A.EVENT_STATUS = dconv_u8_u16(d_m(:,7 + (4:5)));
    IMU.EVENT_A.TIME_OFFSET_0 = dconv_u8_u16(d_m(:,7 + (6:7)));
    IMU.EVENT_A.TIME_OFFSET_1 = dconv_u8_u16(d_m(:,7 + (8:9)));
    IMU.EVENT_A.TIME_OFFSET_2 = dconv_u8_u16(d_m(:,7 + (10:11)));
    IMU.EVENT_A.TIME_OFFSET_3 = dconv_u8_u16(d_m(:,7 + (12:13)));
    IMU.EVENT_A.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.EVENT_A.CRC_CALC = NaN(N_m,1);
    IMU.EVENT_A.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));

end

%% EVENT_B, MSG=25, page 56 << NOT CHECKED >>
clear N_m l_m d_m;

ok_m = MSG==25;
N_m = sum(ok_m);
if N_m==0
    disp('No data in field --> EVENT_B');
else
    disp('Adding field --> EVENT_B');
    l_m = 14 + 5 + 3; % nanmedian(LEN(ok_m));
    try
        d_m = ddec(repmat(ii_index(ok_m)',1,(l_m+1)) + repmat(0:l_m,N_m,1));
    catch
        ii_ind_ok = ii_index(ok_m)';
        disp(['Clipping last packet, MSG = ', num2str(ddec(ii_ind_ok(1)+2))]);
        d_m = ddec(repmat(ii_ind_ok(1:end-1),1,(l_m+1)) + repmat(0:l_m,N_m-1,1));
        N_m = N_m-1;
    end
    if size(d_m,2)==1, d_m = d_m'; end
    
    IMU.EVENT_B.TIME_STAMP = dconv_u8_u32(d_m(:,7 + (0:3)));    % [us] since power on
    IMU.EVENT_B.EVENT_STATUS = dconv_u8_u16(d_m(:,7 + (4:5)));
    IMU.EVENT_B.TIME_OFFSET_0 = dconv_u8_u16(d_m(:,7 + (6:7)));
    IMU.EVENT_B.TIME_OFFSET_1 = dconv_u8_u16(d_m(:,7 + (8:9)));
    IMU.EVENT_B.TIME_OFFSET_2 = dconv_u8_u16(d_m(:,7 + (10:11)));
    IMU.EVENT_B.TIME_OFFSET_3 = dconv_u8_u16(d_m(:,7 + (12:13)));
    IMU.EVENT_B.CRC = dconv_u8_u16(d_m(:,l_m + (-1:0)));
    IMU.EVENT_B.CRC_CALC = CalcCRC_fast(d_m(:,3:end-3));

end



%%
if save_mat
    save(fname_out,'IMU','-v7.3');
end

end




function d16 = dconv_u8_u16(d8)
% d16 = double(d8(:,1)) + bitshift(double(d8(:,2)),8);
d16 = uint16(d8(:,1)) + bitshift(uint16(d8(:,2)),8);
end


% function d16 = dconv_u8_u16_DD(d8)
% d16 = double(d8(:,1)) + bitshift(double(d8(:,2)),8);
% % d16 = uint16(d8(:,1)) + bitshift(uint16(d8(:,2)),8);
% end


function d32 = dconv_u8_u32(d8)
d32 = double(d8(:,1)) + bitshift(double(d8(:,2)),8) + bitshift(double(d8(:,3)),16) + bitshift(double(d8(:,4)),24);
end

function f32 = dconv_u8_float32(d8)
d8 = uint8(d8);
[~,n] = size(d8);
if n~=4, error('Incorrect data input type (4 bytes required)!'); end
d32 = uint32(d8(:,1)) + bitshift(uint32(d8(:,2)),8) + bitshift(uint32(d8(:,3)),16) + bitshift(uint32(d8(:,4)),24);
f32 = double(typecast(d32,'single'));
end

% function f32 = dconv_u8_float32(d8)
% d8 = uint8(d8);
% [m,n] = size(d8);
% if n~=4, error('Incorrect data input type (4 bytes required)!'); end
% f32 = NaN(m,1);
% for i = 1:m
%     f32(i) = typecast(d8(i,:),'single');
% end
% end

function f64 = dconv_u8_float64(d8)
d8 = uint8(d8);
[m,n] = size(d8);
if n~=8, error('Incorrect data input type (8 bytes required)!'); end
f64 = NaN(m,1);
for i = 1:m
    f64(i) = typecast(d8(i,:),'double');
end
end




% CRC calculated using Look Up Table (Faster).  Vectorized version for 100x
% speed increase...
function crc_lut = CalcCRC_fast(BytesBuffer_col)
BytesBuffer_col = uint16(BytesBuffer_col);

N_buff = size(BytesBuffer_col,1);

crc_lut = uint16(zeros(N_buff,1));

crc16LookupTable = uint16(hex2dec({...
    '0000','1189','2312','329B','4624','57AD','6536','74BF','8C48','9DC1','AF5A','BED3','CA6C','DBE5','E97E','F8F7',...
    '1081','0108','3393','221A','56A5','472C','75B7','643E','9CC9','8D40','BFDB','AE52','DAED','CB64','F9FF','E876',...
    '2102','308B','0210','1399','6726','76AF','4434','55BD','AD4A','BCC3','8E58','9FD1','EB6E','FAE7','C87C','D9F5',...
    '3183','200A','1291','0318','77A7','662E','54B5','453C','BDCB','AC42','9ED9','8F50','FBEF','EA66','D8FD','C974',...
    '4204','538D','6116','709F','0420','15A9','2732','36BB','CE4C','DFC5','ED5E','FCD7','8868','99E1','AB7A','BAF3',...
    '5285','430C','7197','601E','14A1','0528','37B3','263A','DECD','CF44','FDDF','EC56','98E9','8960','BBFB','AA72',...
    '6306','728F','4014','519D','2522','34AB','0630','17B9','EF4E','FEC7','CC5C','DDD5','A96A','B8E3','8A78','9BF1',...
    '7387','620E','5095','411C','35A3','242A','16B1','0738','FFCF','EE46','DCDD','CD54','B9EB','A862','9AF9','8B70',...
    '8408','9581','A71A','B693','C22C','D3A5','E13E','F0B7','0840','19C9','2B52','3ADB','4E64','5FED','6D76','7CFF',...
    '9489','8500','B79B','A612','D2AD','C324','F1BF','E036','18C1','0948','3BD3','2A5A','5EE5','4F6C','7DF7','6C7E',...
    'A50A','B483','8618','9791','E32E','F2A7','C03C','D1B5','2942','38CB','0A50','1BD9','6F66','7EEF','4C74','5DFD',...
    'B58B','A402','9699','8710','F3AF','E226','D0BD','C134','39C3','284A','1AD1','0B58','7FE7','6E6E','5CF5','4D7C',...
    'C60C','D785','E51E','F497','8028','91A1','A33A','B2B3','4A44','5BCD','6956','78DF','0C60','1DE9','2F72','3EFB',...
    'D68D','C704','F59F','E416','90A9','8120','B3BB','A232','5AC5','4B4C','79D7','685E','1CE1','0D68','3FF3','2E7A',...
    'E70E','F687','C41C','D595','A12A','B0A3','8238','93B1','6B46','7ACF','4854','59DD','2D62','3CEB','0E70','1FF9',...
    'F78F','E606','D49D','C514','B1AB','A022','92B9','8330','7BC7','6A4E','58D5','495C','3DE3','2C6A','1EF1','0F78'}));

bufferSize = size(BytesBuffer_col,2);

xFF = uint16(255*ones(N_buff,1)); % hex2dec('FF');

% For each byte in the buffer
for i_buff = 1:bufferSize
    % Update the current CRC
    index = bitand( bitxor(BytesBuffer_col(:,i_buff),crc_lut) , xFF ); 
    crc_lut = bitxor(crc16LookupTable(index+1),bitshift(crc_lut,-8));
end

% % Convert to double
% crc_lut = double(crc_lut);

end




% % Depreciated...  see above
% % CRC calculated using Look Up Table (Faster than calculation)
% function crc_lut = CalcCRC(BytesBuffer)
% crc_lut = uint16(0);
% crc16LookupTable = uint16(hex2dec({...
%     '0000','1189','2312','329B','4624','57AD','6536','74BF','8C48','9DC1','AF5A','BED3','CA6C','DBE5','E97E','F8F7',...
%     '1081','0108','3393','221A','56A5','472C','75B7','643E','9CC9','8D40','BFDB','AE52','DAED','CB64','F9FF','E876',...
%     '2102','308B','0210','1399','6726','76AF','4434','55BD','AD4A','BCC3','8E58','9FD1','EB6E','FAE7','C87C','D9F5',...
%     '3183','200A','1291','0318','77A7','662E','54B5','453C','BDCB','AC42','9ED9','8F50','FBEF','EA66','D8FD','C974',...
%     '4204','538D','6116','709F','0420','15A9','2732','36BB','CE4C','DFC5','ED5E','FCD7','8868','99E1','AB7A','BAF3',...
%     '5285','430C','7197','601E','14A1','0528','37B3','263A','DECD','CF44','FDDF','EC56','98E9','8960','BBFB','AA72',...
%     '6306','728F','4014','519D','2522','34AB','0630','17B9','EF4E','FEC7','CC5C','DDD5','A96A','B8E3','8A78','9BF1',...
%     '7387','620E','5095','411C','35A3','242A','16B1','0738','FFCF','EE46','DCDD','CD54','B9EB','A862','9AF9','8B70',...
%     '8408','9581','A71A','B693','C22C','D3A5','E13E','F0B7','0840','19C9','2B52','3ADB','4E64','5FED','6D76','7CFF',...
%     '9489','8500','B79B','A612','D2AD','C324','F1BF','E036','18C1','0948','3BD3','2A5A','5EE5','4F6C','7DF7','6C7E',...
%     'A50A','B483','8618','9791','E32E','F2A7','C03C','D1B5','2942','38CB','0A50','1BD9','6F66','7EEF','4C74','5DFD',...
%     'B58B','A402','9699','8710','F3AF','E226','D0BD','C134','39C3','284A','1AD1','0B58','7FE7','6E6E','5CF5','4D7C',...
%     'C60C','D785','E51E','F497','8028','91A1','A33A','B2B3','4A44','5BCD','6956','78DF','0C60','1DE9','2F72','3EFB',...
%     'D68D','C704','F59F','E416','90A9','8120','B3BB','A232','5AC5','4B4C','79D7','685E','1CE1','0D68','3FF3','2E7A',...
%     'E70E','F687','C41C','D595','A12A','B0A3','8238','93B1','6B46','7ACF','4854','59DD','2D62','3CEB','0E70','1FF9',...
%     'F78F','E606','D49D','C514','B1AB','A022','92B9','8330','7BC7','6A4E','58D5','495C','3DE3','2C6A','1EF1','0F78'}));
% 
% bufferSize = size(BytesBuffer,2);
% 
% % For each byte in the buffer
% for i_buff = 1:bufferSize
%     % Update the current CRC
%     index = bitand( bitxor(uint16(BytesBuffer(i_buff)),crc_lut) , uint16(255) ); % hex2dec('FF');
%     crc_lut = bitxor(crc16LookupTable(index+1),bitshift(crc_lut,-8));
% end
% end












