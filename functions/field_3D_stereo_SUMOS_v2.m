%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%---------Stereo-------%%%%%%%%%%%%%%%%%%%
%%%%%%%%Reconstructed 3D wave field elevation%%%%%%%
%%%%%%%%%%%-------SUMOS MISION 2021-------%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%The 3D point cloud is already computed using the WASS pipeline (Bergamasco
%2017)
% This script is written for reprojection
% Camera frame of coordinate ->World frame of coordinate
% and
% Save on NETCDF file the associated 3D field
% (c) g.marechal march 2021
clc;clear;close all;clear path

set(0,'defaultAxesFontName', 'Serif')
set(0,'defaultTextFontN', 'Serif')
set(0,'DefaultAxesFontSize',16)
set(0,'DefaultTextFontSize',16)
set(0,'defaultfigurecolor',[1 1 1]*1)
scrsz = get(0,'ScreenSize');
%%
%fonctions_path='F:\MATLAB\functions'
fonction_path='/media/gmarecha/Dell USB/MATLAB/functions'
cd (fonctions_path)
%% Specific data for SUMOS mission
% Rotation matrices to switch between the camera coordinate frame and the
% SBG coordinate frame
cx=cosd(90-34.44-2.66);sx=sind(90-34.44-2.66);
cz=cosd(1.68);sz=sind(1.68);
M_Cam2SBG=[cz sz 0;-sz cz 0;0 0 1]*[ 1 0 0;0 cx +sx;0 -sx cx]*[-1,0,0;0,-1,0;0,0,1];

%Lever arms zero SBG towards zero camera
% See in script Dessin Camera 1
LA_Cam1=[796;100;45]*1e-3;

% Baseline between the cameras.
Baseline = 4219e-3;

% Height of cameras above sea level.
Alt_Cameras=11.8;

%% Load the specific acquisition 

acquisition='TIF_20210307_084855';
iidx = 96;
indir=sprintf('E:/STEREO_DATA_DAY_2021_03_07/%s/output/%06d_wd/',acquisition,iidx);

outdir = indir;
framesdir =  [outdir,'/frames/'];
mkdir( framesdir );



%% plot raw image
cd(indir)
img = imread('undistorted/00000001.png');

iptsetpref('ImshowBorder','loose');

imgrgb = repmat(img,[1,1,3]);

    hhfig = figure;
    imshow(imgrgb);
%% IMU
% Read IMU data

%IMU = sbg_load_fast(fname_sbg,save_mat,fname_out);
%indir_SBG='F:\SUMOS';
indir_SBG='/media/gmarecha/Dell USB/SUMOS'
infile_SBG='sbg_20210304_060000.dat'; %MARC shows low wave height in the Guld of Biscay the 03.04.21

%S=load(fullfile(indir_SBG,infile_SBG));

indir_frames=sprintf('E:/STEREO_DATA_DAY_2021_03_07/%s/cam0/',acquisition); %CORRECTION NEEDED HERE ->the date

dis_indir_frames=ls(sprintf('%s%06d*',indir_frames,iidx));
S=sbg_load_fast(fullfile(indir_SBG,infile_SBG),0);%P.Sutherland function

S = sbg_imu_time_fields(S);

%%%%%%%%%%
%extract struct data at specific time
%%%%%%%%%%
EBTS=S.EVENT_B.TS_fix;
t  =interp1(S.UTC_TIME.TS_fix,S.UTC_TIME.time,EBTS);
Lon=interp1(S.EKF_NAV.TS_fix,S.EKF_NAV.LONGITUDE,EBTS);
Lat=interp1(S.EKF_NAV.TS_fix,S.EKF_NAV.LATITUDE ,EBTS);
Alt=interp1(S.EKF_NAV.TS_fix,S.EKF_NAV.ALTITUDE ,EBTS);
Q0 =interp1(S.EKF_QUAT.TS_fix,S.EKF_QUAT.Q0,EBTS);
Q1 =interp1(S.EKF_QUAT.TS_fix,S.EKF_QUAT.Q1,EBTS);
Q2 =interp1(S.EKF_QUAT.TS_fix,S.EKF_QUAT.Q2,EBTS);
Q3 =interp1(S.EKF_QUAT.TS_fix,S.EKF_QUAT.Q3,EBTS);

%save data in matlab format for the frame
cd(indir)
delete('20210307_080000.mat')
save(fullfile(indir,strrep(infile_SBG,'dat','mat')),'t','Lon','Lat','Alt','Q0','Q1','Q2','Q3');
S=load(fullfile(indir,'sbg_20210307_080000.mat'));

ts=sscanf(dis_indir_frames,'%d_%ld_%d.tif'); %extract the timestamps in the filename
ts=ts(2)/1e7/86400+datenum(1601,1,1);
[~,imin]=min(abs(ts-S.t));



cd(fonctions_path)
% SBG information
WGS=[S.Lat(imin);S.Lon(imin);S.Alt(imin)];
Q  =[S.Q0(imin);S.Q1(imin);S.Q2(imin);S.Q3(imin)];

% Construct the DCM from the SBG quaternions.

DCM=[2*(Q(1)^2+Q(2)^2)-1    ,2*(Q(2)*Q(3)-Q(1)*Q(4)), 2*(Q(1)*Q(3)+Q(2)*Q(4));...
     2*(Q(2)*Q(3)+Q(1)*Q(4)),2*(Q(1)^2+Q(3)^2)-1    , 2*(Q(3)*Q(4)-Q(1)*Q(2));...
     2*(Q(2)*Q(4)-Q(1)*Q(3)),2*(Q(3)*Q(4)+Q(1)*Q(2)), 2*(Q(1)^2+Q(4)^2)-1   ];
 
 DCM=[1,0,0;0,1,0;0,0,1];

%% cloud points
cd(indir)
addpath(fonctions_path)
mesh = load_camera_mesh();%The reconstructed point cloud (3xNpoints)
cd(fonctions_path)
Nd=1;

mesh=mesh(1:end,1:Nd:end);
n_pts=size(mesh,2);

%%
    %apply the transformation btw sbg and camera (rotation + translation)
        %apply the transformation btw sbg and camera (rotation + translation)

    mesh_SBG=M_Cam2SBG*Baseline*mesh+ repmat(LA_Cam1,1,n_pts); %in meter (bacause baseline's unit is meter)
    
    ind=find(mesh_SBG(2,:)<=60); %60m normally
    mesh_SBG=mesh_SBG(:,ind);
    
    mesh_DCM=DCM*mesh_SBG ;
    %WGS coordinates, switching to ENU instead of NED
    mesh_WGS=[mesh_DCM(2,:)/1852/60/cosd(WGS(1));mesh_DCM(1,:)/1852/60;-mesh_DCM(3,:)];
    % Then add the SBG location to the WGS coordinates
    mesh_WGS(1,:)=mesh_WGS(1,:)+WGS(2);
    mesh_WGS(2,:)=mesh_WGS(2,:)+WGS(1);
    mesh_WGS(3,:)=mesh_WGS(3,:)+Alt_Cameras;
    
%% PLOT
    %plot cloud point in ENU coordinate
    %cmap=redblue(256);
    vec=[1:50:size(mesh_DCM,2)];
    figure(2),clf
    colormap jet
    scatter3(mesh_DCM(1,vec)',mesh_DCM(2,vec)',mesh_DCM(3,vec)',1,mesh_DCM(3,vec)');
    axis equal
     %caxis([-2,2])
    colorbar
    xlabel('X[m]')
    ylabel('Y[m]')
  %%
    %plot cloud point in SBG coordinate
    cmap=redblue(256);
    figure(3),clf
    colormap(cmap)
    scatter3(mesh_SBG(1,:),mesh_SBG(2,:),mesh_SBG(3,:),1,mesh_SBG(3,:))
    axis equal
    %caxis([-2,2])
    colorbar
    xlabel('X[m]')
    ylabel('Y[m]')   
    
    %plot cloud point in CAM coordinate
    cmap=redblue(256);
    figure(4),clf
    %colormap(cmap)
    colormap jet
    scatter3(mesh(1,:),mesh(2,:),mesh(3,:),1,mesh(3,:))
    axis equal
    %caxis([-2,2])
    colorbar
    xlabel('X[m]')
    ylabel('Y[m]')     
    
    
 