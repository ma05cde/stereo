%%
clc;clear;close all;

EXE_DIR = '/usr/local/wass/dist/bin/';
MAT_DIR = '/usr/local/wass/matlab/';

addpath(MAT_DIR);

PREPARE_EXE  = [EXE_DIR,'wass_prepare'];
MATCH_EXE    = [EXE_DIR,'wass_match'];
AUTOCAL_EXE  = [EXE_DIR,'wass_autocalibrate'];
STEREO_EXE   = [EXE_DIR,'wass_stereo'];

 RUN_DIR='run_2016-10-17_15h06m30.189sZ';
% RUN_DIR='run_2016-10-17_16h41m31.544sZ';
% RUN_DIR='run_2016-10-19_16h22m14.074sZ';
% RUN_DIR='run_2016-10-21_09h03m32.672sZ';
% RUN_DIR='run_2016-10-24_09h23m18.261sZ';
% RUN_DIR='run_2016-10-24-09h56m39.848sZ';
TEST_ROOT    = ['/home/cercache/project/stereo/DYNATREZ_2016/raw/',RUN_DIR];
%CONFIG_DIR   = ['~/Documents/DYNATREZ_2016/config/'];
CONFIG_DIR   = [TEST_ROOT,'/config/'];
CALIB_DIR    = ['/home/cercache/project/stereo/DYNATREZ_2016/raw/Calib/'];
M3D_DIR      = [TEST_ROOT,'/3D/'];
INPUT_C0_DIR = [TEST_ROOT,'/cam0/'];
INPUT_C0_DIR = [TEST_ROOT,'/Images_Raw/'];
INPUT_C1_DIR = [TEST_ROOT,'/cam1/'];
INPUT_C1_DIR = [TEST_ROOT,'/Images_Raw/'];
OUT_DIR      = [TEST_ROOT,'/output/'];

cd(TEST_ROOT)
%%
% Prepare runs 

% Config files
% cd ../Calib
% calib2xml('Calib_Results_stereo.mat');
% !cp ../Calib/*.xml config/.
% !cp -r ~/SOFT/WASS/wass/test/WASS_TEST/W07/config .
% !cp ../Calib/*.xml config/.
% %!rm config/Grid.mat
% eval(['!mkdir -p ',M3D_DIR]);
% cd(TEST_ROOT)

%%
% Sanity checks

assert( exist(EXE_DIR','dir')==7, sprintf('%s does not exists.', EXE_DIR ) );
assert( exist(PREPARE_EXE','file')==2, sprintf('%s does not exists.', PREPARE_EXE ) );
assert( exist(MATCH_EXE','file')==2, sprintf('%s does not exists.', MATCH_EXE ) );
assert( exist(AUTOCAL_EXE','file')==2, sprintf('%s does not exists.', AUTOCAL_EXE ) );
assert( exist(STEREO_EXE','file')==2, sprintf('%s does not exists.', STEREO_EXE ) );

assert( exist(TEST_ROOT','dir')==7, sprintf('%s does not exists.', TEST_ROOT ) );
assert( exist(CONFIG_DIR','dir')==7, sprintf('%s does not exists.', CONFIG_DIR ) );
assert( exist(M3D_DIR','dir')==7, sprintf('%s does not exists.', M3D_DIR ) );
assert( exist(INPUT_C0_DIR','dir')==7, sprintf('%s does not exists.', INPUT_C0_DIR ) );
assert( exist(INPUT_C1_DIR','dir')==7, sprintf('%s does not exists.', INPUT_C1_DIR ) );

%%
% List frames

input_frames = cell(0);

cam0_frames = dir([INPUT_C0_DIR,'*_01.*']);
kk=1;
for ii=1:numel(cam0_frames)
    if cam0_frames(ii).bytes > 0 && cam0_frames(ii).isdir == 0
        input_frames{kk} = struct('Cam0', [INPUT_C0_DIR,cam0_frames(ii).name], ...
                                  'wd', sprintf('%s%06d_wd/',OUT_DIR,kk) );
        kk=kk+1;
    end
end
clear('cam0_frames');
cam1_frames = dir([INPUT_C0_DIR,'*_02.*']);
kk=1;
for ii=1:numel(cam1_frames)
    if cam1_frames(ii).bytes > 0 && cam1_frames(ii).isdir == 0
        input_frames{kk}.Cam1 = [INPUT_C1_DIR, cam1_frames(ii).name];
        kk=kk+1;
    end
end

fprintf('%d stereo frames found.\n', numel( input_frames ) );

%%
% Prepare output directory

if exist(OUT_DIR','dir')==7
    fprintf('%s already exists, removing it\n', OUT_DIR );
    %assert( rmdir( OUT_DIR, 's' )==1, 'Cannot remove');
end
fprintf('Creating %s\n', OUT_DIR );
mkdir( OUT_DIR );
    
%% 
% Run WASS prepare

fprintf('***************************************************\n');
fprintf('**  RUNNING wass_prepare                      *****\n');
fprintf('***************************************************\n');
p = getenv('LD_LIBRARY_PATH');
p = [p ':/usr/lib/x86_64-linux-gnu']
setenv('LD_LIBARARY_PATH', p);


tic;

for ii=1:numel(input_frames)
    %assert( 
	system( [PREPARE_EXE, ' --workdir ', input_frames{ii}.wd, ' --calibdir ', CONFIG_DIR, ...
            ' --c0 ', input_frames{ii}.Cam0, ' --c1 ', input_frames{ii}.Cam1] );% == 0, 'component exited with non-zero return code');
end

fprintf('***************************************************\n');
fprintf(' Done in %f secs.\n', toc );

%% 
% Run WASS match

fprintf('***************************************************\n');
fprintf('**  RUNNING wass_match                        *****\n');
fprintf('***************************************************\n');

tic;

for ii=1:numel(input_frames)
%    assert( system( [MATCH_EXE, ' ', CONFIG_DIR, 'matcher_config.txt ', input_frames{ii}.wd] ) == 0, 'component exited with non-zero return code');
end

fprintf('***************************************************\n');
fprintf(' Done in %f secs.\n', toc );

%status = verify_matcher( input_frames, CONFIG_DIR );
%assert( strcmp(status, 'ok' ), ['wass_match failed: ',status]);

%%
% Run WASS autocalibrate

fprintf('***************************************************\n');
fprintf('**  RUNNING wass_autocalibrate                *****\n');
fprintf('***************************************************\n');

tic;

% create workspaces file
fid = fopen( [OUT_DIR,'/workspaces.txt'], 'w' );
for ii=1:numel(input_frames)
    fwrite(fid, [input_frames{ii}.wd,10] );
end
fclose(fid);

%assert( system( [AUTOCAL_EXE, ' ', OUT_DIR,'workspaces.txt'] ) == 0, 'component exited with non-zero return code');

%% 
% Run WASS stereo

fprintf('***************************************************\n');
fprintf('**  RUNNING wass_stereo                       *****\n');
fprintf('***************************************************\n');

tic;

for ii=1:numel(input_frames)
%   system( [STEREO_EXE, ' ', CONFIG_DIR, 'stereo_config.txt ', input_frames{ii}.wd] );
    %assert( system( [STEREO_EXE, ' ', CONFIG_DIR, 'stereo_config.txt ', input_frames{ii}.wd] ) == 0, 'component exited with non-zero return code');
end

fprintf('***************************************************\n');
fprintf(' Done in %f secs.\n', toc );


%%
% Check 3D data
% 
% fprintf('***************************************************\n');
% fprintf('**  Verifying 3D point clouds                 *****\n');
% fprintf('***************************************************\n');
% %verify_meshes( input_frames, CONFIG_DIR, M3D_DIR);
% fprintf('***************************************************\n');
% fprintf(' ALL TESTS OK!\n');

%%
% Create Surface_%.nc
!rm 3D/*
for ii=1:numel(input_frames)
    %wdir = input_frames{ii}.wd;
    %try 
%        xyzC2nc(OUT_DIR, M3D_DIR, CALIB_DIR, ii)
    %catch me
    %    err=[err,ii-1];
    %end

end
