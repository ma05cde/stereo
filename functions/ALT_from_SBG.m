close all
clear all
clc

%% LOAD all altitudes from SBG (SUMOS 2021)

clc;clear;close all;clear path

set(0,'defaultAxesFontName', 'Serif')
set(0,'defaultTextFontN', 'Serif')
set(0,'DefaultAxesFontSize',16)
set(0,'DefaultTextFontSize',16)
set(0,'defaultfigurecolor',[1 1 1]*1)
scrsz = get(0,'ScreenSize');
%%
ALT_all=0;
%%
date=linspace(15,28,14);
hour=linspace(6,21,16); %no data during night (sbg data acquired during camera acquisition)

%%
fonctions_path='C:\Users\stereo\Desktop\MATLAB_SUMOS_Gmare\functions'
addpath(fonctions_path)

indir_SBG='C:\Users\stereo\Desktop\SBG_data\';
for i_date=1:length(date)
    for i_hour=1:length(hour)

infile_SBG=sprintf('sbg_202102%02d_%02d0000.dat',date(i_date),hour(i_hour))
%infile_SBG='sbg_20210210_163447_calibration_polarimetric.dat';
% 
% %S=load(fullfile(indir_SBG,infile_SBG));
% 

S=sbg_load_fast(fullfile(indir_SBG,infile_SBG),0);%P.Sutherland function %verif here !!!
% 
S = sbg_imu_time_fields(S); %Peter Sutherland's function : strcuture that contain SBG info.
%there is one SBG file per hour -> reccord over 2 hours, needs to load 2
%SBG file (ex : 01:00 and 02:00.)

% %%%%%%%%%%
% %extract struct data at specific time
% %%%%%%%%%%
 
EBTS=S.EVENT_B.TS_fix;
t  =interp1(S.UTC_TIME.TS_fix,S.UTC_TIME.time,EBTS);
Lon=interp1(S.EKF_NAV.TS_fix,S.EKF_NAV.LONGITUDE,EBTS);
Lat=interp1(S.EKF_NAV.TS_fix,S.EKF_NAV.LATITUDE ,EBTS);
Alt=interp1(S.EKF_NAV.TS_fix,S.EKF_NAV.ALTITUDE ,EBTS);
Q0 =interp1(S.EKF_QUAT.TS_fix,S.EKF_QUAT.Q0,EBTS);
Q1 =interp1(S.EKF_QUAT.TS_fix,S.EKF_QUAT.Q1,EBTS);
Q2 =interp1(S.EKF_QUAT.TS_fix,S.EKF_QUAT.Q2,EBTS);
Q3 =interp1(S.EKF_QUAT.TS_fix,S.EKF_QUAT.Q3,EBTS);
ALT_all=[ALT_all;Alt];
length(ALT_all)
    end
end

%%
figure(1),clf
plot(ALT_all,'x')
hold on
infile_SBG='sbg_20210223_080000.dat';
% 
% %S=load(fullfile(indir_SBG,infile_SBG));
% 

S=sbg_load_fast(fullfile(indir_SBG,infile_SBG),0);%P.Sutherland function %verif here !!!
% 
S = sbg_imu_time_fields(S); %Peter Sutherland's function : strcuture that contain SBG info.
%there is one SBG file per hour -> reccord over 2 hours, needs to load 2
%SBG file (ex : 01:00 and 02:00.)

% %%%%%%%%%%
% %extract struct data at specific time
% %%%%%%%%%%
 
EBTS=S.EVENT_B.TS_fix;
t  =interp1(S.UTC_TIME.TS_fix,S.UTC_TIME.time,EBTS);
Lon=interp1(S.EKF_NAV.TS_fix,S.EKF_NAV.LONGITUDE,EBTS);
Lat=interp1(S.EKF_NAV.TS_fix,S.EKF_NAV.LATITUDE ,EBTS);
Alt=interp1(S.EKF_NAV.TS_fix,S.EKF_NAV.ALTITUDE ,EBTS);

ALT_all=[ALT_all;Alt];
plot(ALT_all,'r')

%% calc roll


roll=atan((2*Q2.*Q3+2*Q0.*Q1)./(2*Q0.^2+2*Q3.^2-1));
plot(roll*180/pi)