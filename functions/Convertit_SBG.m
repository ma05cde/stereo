iidx=219;
indir = sprintf('E:/STEREO_DATA_DAY_2021_02_19/TIF_20210219_082101/output/%06d_wd',iidx);
infile='sbg_20210219_080000.dat';

%S=sbg_load_fast(fullfile(indir,infile),0);
indir_SBG='F:/';
S=sbg_load_fast(fullfile(indir_SBG,infile),0);
S = sbg_imu_time_fields(S);

EBTS=S.EVENT_B.TS_fix;
t  =interp1(S.UTC_TIME.TS_fix,S.UTC_TIME.time,EBTS);
Lon=interp1(S.EKF_NAV.TS_fix,S.EKF_NAV.LONGITUDE,EBTS);
Lat=interp1(S.EKF_NAV.TS_fix,S.EKF_NAV.LATITUDE ,EBTS);
Alt=interp1(S.EKF_NAV.TS_fix,S.EKF_NAV.ALTITUDE ,EBTS);
Q0 =interp1(S.EKF_QUAT.TS_fix,S.EKF_QUAT.Q0,EBTS);
Q1 =interp1(S.EKF_QUAT.TS_fix,S.EKF_QUAT.Q1,EBTS);
Q2 =interp1(S.EKF_QUAT.TS_fix,S.EKF_QUAT.Q2,EBTS);
Q3 =interp1(S.EKF_QUAT.TS_fix,S.EKF_QUAT.Q3,EBTS);

save(fullfile(indir,strrep(infile,'dat','mat')),'t','Lon','Lat','Alt','Q0','Q1','Q2','Q3');
