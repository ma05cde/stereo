clc;clear;close all;clear path

set(0,'defaultAxesFontName', 'Serif')
set(0,'defaultTextFontN', 'Serif')
set(0,'DefaultAxesFontSize',16)
set(0,'DefaultTextFontSize',16)
set(0,'defaultfigurecolor',[1 1 1]*1)
scrsz = get(0,'ScreenSize');
%%
% Rotation matrices to switch between the camera coordinate frame and the
% SBG coordinate frame
cx=cosd(90-34.44-2.66);sx=sind(90-34.44-2.66);
cz=cosd(1.68);sz=sind(1.68);
M_Cam2SBG=[cz sz 0;-sz cz 0;0 0 1]*[ 1 0 0;0 cx +sx;0 -sx cx]*[-1,0,0;0,-1,0;0,0,1];

%Lever arms zero SBG towards zero camera
% See in script Dessin Camera 1
LA_Cam1=[796;100;45]*1e-3;

% Baseline between the cameras.
Baseline = 4219e-3;

% Height of cameras above sea level.
Alt_Cameras=11.8;

%%
% %cd('/media/gmarecha/Dell
% USB/WASS_ubuntu/sensibilitÃ©_wass/wd_outputs_dilate_20')
iidx = 201;
indir = sprintf('E:/STEREO_DATA_DAY_2021_02_19/TIF_20210219_082101/output/%06d_wd',iidx);
outdir = indir;
framesdir =  [outdir,'/frames/'];
mkdir( framesdir );

%%
% Read IMU data


%IMU = sbg_load_fast(fname_sbg,save_mat,fname_out);
indir_SBG='F:';
S=load(fullfile(indir_SBG,'sbg_20210219_080000.mat'));

fn='000219_00132581965314268178_01.tif';
ts=sscanf(fn,'%d_%ld_%d.tif');
ts=ts(2)/1e7/86400+datenum(1601,1,1);
[~,imin]=min(abs(ts-S.t));

% SBG information
WGS=[S.Lat(imin);S.Lon(imin);S.Alt(imin)];
Q  =[S.Q0(imin);S.Q1(imin);S.Q2(imin);S.Q3(imin)];

% Construct the DCM from the SBG quaternions.

DCM=[2*(Q(1)^2+Q(2)^2)-1    ,2*(Q(2)*Q(3)-Q(1)*Q(4)), 2*(Q(1)*Q(3)+Q(2)*Q(4));...
     2*(Q(2)*Q(3)+Q(1)*Q(4)),2*(Q(1)^2+Q(3)^2)-1    , 2*(Q(3)*Q(4)-Q(1)*Q(2));...
     2*(Q(2)*Q(4)-Q(1)*Q(3)),2*(Q(3)*Q(4)+Q(1)*Q(2)), 2*(Q(1)^2+Q(4)^2)-1   ];

%WINDOWS
%addpath 'F:\WASS_ubuntu\wass\test'
%addpath 'F:\WASS_ubuntu\wass\matlab'
%addpath 'F:\MATLAB\functions'
%LINUX
% addpath '/media/gmarecha/Dell USB/WASS_ubuntu/sensibilitÃ©_wass'
% addpath '/media/gmarecha/Dell USB/WASS_ubuntu/wass/matlab'
% addpath '/media/gmarecha/Dell USB/MATLAB/functions'

    %workdir = outdir;
  
%     was_unzipped = 0;
%         
%     if exist(workdir','dir') ~= 7
%             
%         if exist(workdirzip','file')==2
%             prev = cd(outdir);
%             unzip(workdirzip);
%             cd(prev);
%             was_unzipped = 1;
%         else
%             fprintf( '%s does not exist, exiting\n', workdir);
%             
%         end
%     end
    
%     fprintf( '%s\n',workdir);
    
%     prev = cd(workdir);


%% Load the data.
    mesh = load_camera_mesh(fullfile(indir,'mesh_cam.xyzC'));%The reconstructed point cloud (3xNpoints)

    Nd=1;
    mesh=mesh(1:end,1:Nd:end);
    n_pts=size(mesh,2);

%%
    %apply the transformation btw sbg and camera (rotation + translation)
        %apply the transformation btw sbg and camera (rotation + translation)

    mesh_SBG=M_Cam2SBG*Baseline*mesh+ repmat(LA_Cam1,1,n_pts);
    
    ind=find(mesh_SBG(2,:)<=60);
    mesh_SBG=mesh_SBG(:,ind);
    
    mesh_DCM=DCM*mesh_SBG;
    %WGS coordinates, switching to ENU instead of NED
    mesh_WGS=[mesh_DCM(2,:)/1852/60/cosd(WGS(1));mesh_DCM(1,:)/1852/60;-mesh_DCM(3,:)];
    % Then add the SBG location to the WGS coordinates
    mesh_WGS(1,:)=mesh_WGS(1,:)+WGS(2);
    mesh_WGS(2,:)=mesh_WGS(2,:)+WGS(1);
    mesh_WGS(3,:)=mesh_WGS(3,:)+Alt_Cameras;
    
    
    
    
%% PLOT
    %plot cloud point in ENU coordinate
    cmap=redblue(256);
    figure(2),clf
    colormap(cmap)
    scatter3(mesh_DCM(1,:),mesh_DCM(2,:),Alt_Cameras-mesh_DCM(3,:),1,Alt_Cameras-mesh_DCM(3,:))
    axis equal
    %caxis([-2,2])
    colorbar
    xlabel('X[m]')
    ylabel('Y[m]')
  
    %plot cloud point in SBG coordinate
    cmap=redblue(256);
    figure(3),clf
    colormap(cmap)
    scatter3(mesh_SBG(1,:),mesh_SBG(2,:),mesh_SBG(3,:),1,mesh_SBG(3,:))
    axis equal
    %caxis([-2,2])
    colorbar
    xlabel('X[m]')
    ylabel('Y[m]')   
    
    %plot cloud point in CAM coordinate
    cmap=redblue(256);
    figure(4),clf
    colormap(cmap)
    scatter3(mesh(1,:),mesh(2,:),mesh(3,:),1,mesh(3,:))
    axis equal
    %caxis([-2,2])
    colorbar
    xlabel('X[m]')
    ylabel('Y[m]')     
    
    
    
    %% # NEXT IS BULLSHIT !!!!
    %% try a fit with a plane
    x=mesh_DCM(1,:)';
    y=mesh_DCM(2,:)';
    z=mesh_DCM(3,:)';
    DM = [x, y, ones(size(z))];                             % Design Matrix
    B = DM\z;                                               % Estimate Parameters
    [X,Y] = meshgrid(linspace(min(x),max(x),size(x,2)), linspace(min(y),max(y),size(y,2)));
    Z = B(1)*X + B(2)*Y + B(3)*ones(size(X));
    plane=[B(1);B(2);B(3);];
    
    %%

    Npts = size(mesh_DCM,2);
    elevations = dot(mesh_DCM, repmat(plane,1,Npts) );
    
    
    dot( [mesh_DCM;ones(1,size(mesh_DCM,2))], repmat(plane,1,Npts) );
    
    % fix scale and z direction
    
    pt2d = P1 * [mesh_DCM;ones(1,size(mesh_DCM,2))];
    pt2d = pt2d ./ repmat( pt2d(3,:),3,1);

    hs = scatter( pt2d(1,:), pt2d(2,:),1,elevations,'.');
    %%
    %mesh=SBG_to_world;
    cd 'F:\WASS_ubuntu\sensibilité_wass\wd_outputsbis_dilate_2\';
    %[mesh, R, T] = load_camera_mesh_and_align_plane( datadir, 100, 1, 'plane.txt' );
    plane = importdata('plane.txt');%the 4 parameters that define a plane
    P1 = importdata('P1cam.txt');
    
    %P1_second=DCM*P1;
    img = imread('undistorted/00000001.png');
    cd(prev);
    
    

    Npts = size(mesh_DCM,2);
    elevations = dot( [mesh_DCM;ones(1,size(mesh_DCM,2))], repmat(plane,1,Npts) );
    
    % fix scale and z direction
    elevations = -elevations * 4219/1000; %I edited 2.5, remplace it by the distance between the two cameras (4219mm)
    %%
    % project
    
    pt2d = P1 * [mesh_DCM;ones(1,size(mesh_DCM,2))];
    pt2d = pt2d ./ repmat( pt2d(3,:),3,1);

    %%
    
    close all;
    iptsetpref('ImshowBorder','loose');
    imgrgb = repmat(img,[1,1,3]);
    hhfig = figure;
    imshow(imgrgb);
    hold on;
    hs = scatter( pt2d(1,:), pt2d(2,:),1,elevations,'.');
    
    hs.MarkerEdgeAlpha=0.05;
    caxis( [-2 2] );
    title(sprintf('Frame %06d - %07d points', iidx, Npts) );
    c = colorbar;
    ylabel(c,'Elevation (m)');
    
    axis ij;
    cmap=redblue;
    colormap(cmap)
   

    crop = 20;
    hhfig.Position = [0 0 1440+crop*2 1080+crop*2];
    drawnow;
    ii = getframe(hhfig);
    ii = ii.cdata;
    ii = ii( crop-10:(end-crop-10), crop+5:(end-crop-5), : );
    

    imwrite(ii,sprintf('%s/frm%06d.png',framesdir,iidx));
    %caxis([-2,2])
%%
% DCM_Q=[2*q0.^2+2*q1.^2-1, 2*q1.*q2-2.*q0.*q3,2*q0.*q2+2*q1.*q3; ...
%     2*q1.*q2+2*q0.*q3, 2*q0.^2+2*q2.^2-1,2*q2.*q3-2*q0.*q1; ...
%     2.*q1.*q3-2.*q0.*q2,2.*q2.*q3+2.*q0.*q1, 2.*q0.^2+2.*q3.^2-1];

alpha=58*pi/180; %angle btw x-cam and x-sbg
beta=180*pi/180; %angle btw y-cam and y-sbg
gamma=-1.68*pi/180; %angle btw z-cam and z-sbg

%%%%%%%%%%%%%%%%%%%%
%Measurements btw 0-cam and 0-SBG
%%%%%%%%%%%%%%%%%%%%
LAx=-792.15E-3; %in [m]
LAy=-125.5E-3; %in [m]
LAz=-213.5E-3; %in [m]
LA=[LAx,LAy,LAz];





%method 1 (mine):
Rx=[1,0,0;...
       0,cos(alpha), -sin(alpha);...
        0,sin(alpha),cos(alpha)];
     
% Ry=[cos(gamma),0,sin(gamma);...
%                         0,1, 0;...
%         -sin(gamma),0,cos(gamma)];

Rz=[cos(beta),-sin(beta),0;...
    sin(beta),cos(beta),0;...
        0,0,1];
    
Rzprim=[cos(gamma),-sin(gamma),0;...
    sin(gamma),cos(gamma),0;...
        0,0,1];
    
R_SBGCAM=Rz*Rx*Rzprim;
rot_elevations1=R_SBGCAM*mesh;
%% 
%%%%angle%%%
phi0_SBGCAM=(90-31.8)*(pi/180); %(90-34.4-2.6), measured on board
theta0_SBGCAM=0; % both cam and sbg are align to y axis
psi0_SBGCAM=(0-1.68)*pi/180; %cam and SBG are  perpendicular - a tiny angle for the SBG (not perfectly aligned on the bar)
% 90-1.68

%%%%%%%%%%%%%%%%%%%%
%Measurements btw 0-cam and 0-SBG
%%%%%%%%%%%%%%%%%%%%
LAx=-792.15E-3; %in [m]
LAy=-125.5E-3; %in [m]
LAz=-213.5E-3; %in [m]
LA=[LAx,LAy,LAz];

[Rx_SBGCAM,~,Rz_SBGCAM]=Rot_MAT3(phi0_SBGCAM,theta0_SBGCAM, psi0_SBGCAM);%the 3 rotation matrice btw zero cam and zero sbg

R_CAMSBG=mtimes(Rx_SBGCAM,Rz_SBGCAM); %from the camera frame to the SBG frame (only Rotation !!)

    
%% plot

pt2d = P1 * [mesh;ones(1,size(mesh,2))];
pt2d = pt2d ./ repmat( pt2d(3,:),3,1);
%elevations = dot( [mesh;ones(1,size(mesh,2))], repmat(plane,1,Npts) );
    
rot_elevations=R_CAMSBG*mesh;
shift_elevations=rot_elevations+LA';
world_elevation=DCM*shift_elevations;

%% Louis try !
alpha=58*pi/180; %angle btw x-cam and x-sbg
beta=180*pi/180; %angle btw y-cam and y-sbg
gamma=-1.68*pi/180; %angle btw z-cam and z-sbg

%%%%%%%%%%%%%%%%%%%%
%Measurements btw 0-cam and 0-SBG
%%%%%%%%%%%%%%%%%%%%
LAx=-792.15E-3; %in [m]
LAy=-125.5E-3; %in [m]
LAz=-213.5E-3; %in [m]
LA=[LAx,LAy,LAz];





%method 1 (mine):
Rx=[1,0,0;...
       0,cos(alpha), -sin(alpha);...
        0,sin(alpha),cos(alpha)];
     
% Ry=[cos(gamma),0,sin(gamma);...
%                         0,1, 0;...
%         -sin(gamma),0,cos(gamma)];

Rz=[cos(beta),-sin(beta),0;...
    sin(beta),cos(beta),0;...
        0,0,1];
    
Rzprim=[cos(gamma),-sin(gamma),0;...
    sin(gamma),cos(gamma),0;...
        0,0,1];
    
R_SBGCAM=Rz*Rx*Rzprim;
rot_elevations1=R_SBGCAM*mesh;

% Method2: LOUIS
R_ALL=[-cos(beta), sin(beta)*cos(alpha),-sin(beta)*sin(alpha);...
    0,cos(alpha),sin(alpha);
    sin(beta),cos(beta)*cos(alpha),cos(beta)*cos(alpha)];
%% 4x4
Rx=[1,0,0,0;...
       0,cos(alpha), -sin(alpha);...
        0,sin(alpha),cos(alpha)];
     
% Ry=[cos(gamma),0,sin(gamma);...
%                         0,1, 0;...
%         -sin(gamma),0,cos(gamma)];

Rz=[cos(beta),-sin(beta),0;...
    sin(beta),cos(beta),0;...
        0,0,1];
    
Rzprim=[cos(gamma),-sin(gamma),0;...
    sin(gamma),cos(gamma),0;...
        0,0,1];
    
R_SBGCAM=Rz*Rx*Rzprim;



%% PLOT 3D
MAT_LOUIS=[-1,0,0;...
    0,-cos(alpha),sin(alpha);...
     0,sin(alpha),cos(alpha)]*Rzprim; 
 
% R_ALL=MAT_LOUIS
rot_elevations2=MAT_LOUIS*mesh;

%AND TRANSLATION

%CAM_to_SBG=rot_elevations1+LA';
n_pts = size(mesh,2);

CAM_to_SBG=rot_elevations2+repmat(LA,n_pts,1)';
%% test: rotate also the mean plane
Npts=size(MAT_LOUIS,1);
new_plane = dot( [MAT_LOUIS;ones(1,size(MAT_LOUIS,2))],repmat(plane,1,Npts) );
Npts=size(DCM,1);
new_plane2=dot( [DCM;ones(1,size(DCM,2))], repmat(new_plane,1,Npts) );
    
Npts = size(plane,2);
new_plane =  plane'*[MAT_LOUIS;ones(1,size(MAT_LOUIS,2))];
        
CAM_to_SBG=plane_new+[LA,0]';

%% PLOT mesh 3D
colormap jet
figure(2),clf
subplot(1,2,1)
pcshow(mesh');
xlabel('X_{CAM} [m]')
ylabel('Y_{CAM} [m]')
zlabel('Z_{CAM} [m]')
title ('Camera frame')

box on 
grid minor
subplot(1,2,2)
pcshow(CAM_to_SBG');
xlabel('X_{SBG} [m]')
ylabel('Y_{SBG} [m]')
zlabel('Z_{SBG} [m]')
title ('SBG frame')
colorbar
box on 
grid minor
%%

    Npts = size(mesh,2);
    elevations2 = dot( [mesh;ones(1,size(CAM_to_SBG,2))], repmat(plane,1,Npts) );
    
    % fix scale and z direction
    elevations2 = -elevations2 * 4219/1000; %I edited 2.5, remplace it by the distance between the two cameras (4219mm)
    
    

%% Then apply the DCM

DCM=[-0.9506,0.3103,0.0065;-0.3097,-0.9469,-0.0863;-0.0206,-0.0841,0.9962];

DELTAZ=-12+15.3628;
DELTAZ=0;
DELTAX=0; %if boat is stationnary
DELTAY=0; %if boat is stationnary
%rotation
Rot_SBG_world=DCM*CAM_to_SBG;
%translation
SBG_to_world=Rot_SBG_world+[DELTAX,DELTAY,DELTAZ]';

%%
colormap jet
figure(2),clf
subplot(1,3,1)
pcshow(mesh');
xlabel('X_{CAM} [m]')
ylabel('Y_{CAM} [m]')
zlabel('Z_{CAM} [m]')
title ('Camera frame')

box on 
grid minor
subplot(1,3,2)
pcshow(CAM_to_SBG');
xlabel('X_{SBG} [m]')
ylabel('Y_{SBG} [m]')
zlabel('Z_{SBG} [m]')
title ('SBG frame')
colorbar
box on 
grid minor
subplot(1,3,3)

pcshow(SBG_to_world');
xlabel('X_{SBG} [m]')
ylabel('Y_{SBG} [m]')
zlabel('Z_{SBG} [m]')
title ('World frame')
colorbar
box on 
grid minor

    
    
