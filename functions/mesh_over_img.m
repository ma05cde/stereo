%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%---------Stereo-------%%%%%%%%%%%%%%%%%%%
%%%%%%%%Plot 3D wave field elevation%%%%%%%%%%%%%%%%
%%%%%%%%%%%-------SUMOS MISION 2021-------%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%The 3D point cloud is already computed using the WASS pipeline (Bergamasco
%2017)
%plot the mesh over the *.png image
% (c) g.marechal February 2022
clc;clear;close all;clear path

set(0,'defaultAxesFontName', 'Serif')
set(0,'defaultTextFontN', 'Serif')
set(0,'DefaultAxesFontSize',16)
set(0,'DefaultTextFontSize',16)
set(0,'defaultfigurecolor',[1 1 1]*1)
scrsz = get(0,'ScreenSize');
%%

fonctions_path='C:\Users\stereo\Desktop\MATLAB_SUMOS_Gmare\functions'
addpath(fonctions_path)
%%

acquisition='TIF_20210223_073054';
i_frame=50;%index of frame

iidx=i_frame;
indir=sprintf('E:/STEREO_DATA_DAY_2021_02_23/%s/output/%06d_wd',acquisition,iidx);
outdir = indir;
framesdir =  [outdir,'/frames/'];
mkdir( framesdir );
file_img='/undistorted/00000001.png';
mesh = load_camera_mesh();%The reconstructed point cloud (3xNpoints), which dimension is x,y or z?

Baseline = 4219e-3;% Baseline between the cameras.

%% Plot raw image
cd (indir)
img = imread('undistorted/00000001.png');

iptsetpref('ImshowBorder','loose');

imgrgb = repmat(img,[1,1,3]);
hhfig = figure;
%img = imread(sprintf('%s%s',indir,file_img));

hhfig = figure;
colormap jet
imshow(imgrgb);
hold on
P1=importdata('P1cam.txt');
plane=importdata('plane.txt');
Npts=size(mesh,2);
pt2d=P1*[mesh;ones(1,size(mesh,2))];
pt2d=pt2d./repmat(pt2d(3,:),3,1);
elevations=dot([mesh;ones(1,size(mesh,2))],repmat(plane,1,Npts));
elevations=elevations*(-1)*Baseline;
s1=scatter(pt2d(1,:),pt2d(2,:),1,elevations,'.');
s1.MarkerFaceAlpha=0.01;
s1.MarkerEdgeAlpha=0.01;

colorbar


