function [IMU,h_start] = sbg_imu_time_fields(IMU)
% interpolate everything to seconds since the "top of the hour"

% apply CRC check to all data, and discard those that fail.
disp('Applying CRC');
field_names = fieldnames(IMU);
for i_field = 4:length(field_names)
    try
        nok_CRC = IMU.(field_names{i_field}).CRC~=IMU.(field_names{i_field}).CRC_CALC;
        fn_level2 = fieldnames(IMU.(field_names{i_field}));
        for i_field_l2 = 1:length(fn_level2)
            if length(IMU.(field_names{i_field}).(fn_level2{i_field_l2}))==length(nok_CRC)
                IMU.(field_names{i_field}).(fn_level2{i_field_l2})(nok_CRC) = [];
            end
        end
        disp(['IMU.' field_names{i_field} ' --> num failed CRC: ' num2str(sum(nok_CRC)) '/' num2str(length(nok_CRC))]);
        
    catch
        disp(['Missing CRC  --> ' field_names{i_field}]);
    end
end

% Remove wrapping in internal clock
disp('Unwrapping timing - NOTE: timing could be wrong if differing numbers of clock jumps are present!!');
field_names = fieldnames(IMU);
for i_field = 4:length(field_names)
    ind_rs = find(diff(IMU.(field_names{i_field}).TIME_STAMP)<0*-3E+9);
    IMU.(field_names{i_field}).TS_fix = IMU.(field_names{i_field}).TIME_STAMP;
    if ~isempty(ind_rs)
        for i_step = 1:length(ind_rs)
            IMU.(field_names{i_field}).TS_fix((ind_rs(i_step)+1):end) = IMU.(field_names{i_field}).TS_fix((ind_rs(i_step)+1):end)+2^32;
        end
    end
    disp(['IMU.' field_names{i_field} ' --> num int_clock jumps: ' num2str(length(ind_rs))]);
end

% sync internal clock time
% ([0;diff(IMU.UTC_TIME.GPS_TOW)==1000]);
 %(IMU.UTC_TIME.CRC==IMU.UTC_TIME.CRC_CALC); %&



% IMU.UTC_TIME.time = datenum([IMU.UTC_TIME.YEAR, IMU.UTC_TIME.MONTH, IMU.UTC_TIME.DAY, IMU.UTC_TIME.HOUR, IMU.UTC_TIME.MIN, (IMU.UTC_TIME.SEC + IMU.UTC_TIME.NANOSEC/1E+9)]);
h_start = datenum([min([IMU.UTC_TIME.YEAR, IMU.UTC_TIME.MONTH, IMU.UTC_TIME.DAY, IMU.UTC_TIME.HOUR]) 0 0]);
time_sec = datenum([IMU.UTC_TIME.YEAR, IMU.UTC_TIME.MONTH, IMU.UTC_TIME.DAY, IMU.UTC_TIME.HOUR, IMU.UTC_TIME.MIN, IMU.UTC_TIME.SEC]);
%time_srel = (IMU.UTC_TIME.time-h_start)*86400;

% time in seconds since the start of the hour
IMU.UTC_TIME.t = round((time_sec-h_start)*86400) + IMU.UTC_TIME.NANOSEC/1E+9;


ok_UT = (IMU.UTC_TIME.CLOCK_STATUS==167) & (abs((IMU.UTC_TIME.GPS_TOW/1000-IMU.UTC_TIME.t) - median(IMU.UTC_TIME.GPS_TOW/1000-IMU.UTC_TIME.t)) < 1E-8);



for i_field = 4:length(field_names)
    if ~isempty(IMU.(field_names{i_field}).TS_fix)
        IMU.(field_names{i_field}).t = interp1(IMU.UTC_TIME.TS_fix(ok_UT),IMU.UTC_TIME.t(ok_UT),IMU.(field_names{i_field}).TS_fix);
        IMU.(field_names{i_field}).time = interp1(IMU.UTC_TIME.TS_fix(ok_UT),IMU.UTC_TIME.time(ok_UT),IMU.(field_names{i_field}).TS_fix);
        %disp(field_names{i_field});
    else
        disp(['Field ' field_names{i_field} ' is empty!']);
    end
end


if isfield(IMU,'EKF_QUAT')
    IMU.EKF_QUAT.SOLUTION_STATUS_info.ATTITUDE_VALID = bitget(IMU.EKF_QUAT.SOLUTION_STATUS,4+1);
    IMU.EKF_QUAT.SOLUTION_STATUS_info.HEADING_VALID = bitget(IMU.EKF_QUAT.SOLUTION_STATUS,5+1);
    IMU.EKF_QUAT.SOLUTION_STATUS_info.POSITION_VALID = bitget(IMU.EKF_QUAT.SOLUTION_STATUS,7+1);
    IMU.EKF_QUAT.SOLUTION_STATUS_info.MAG_REF_USED = bitget(IMU.EKF_QUAT.SOLUTION_STATUS,9+1);
end




