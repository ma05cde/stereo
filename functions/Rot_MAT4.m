function [ Rot_x,Rot_y,Rot_z] = Rot_MAT4(phi,theta, psi)
%function returns rotation matrix with respect to x,y and z axis
%angle are in radian
%phi is the angle with repsect to the x-axis (Roll axis)
%theta is the angle with repsect to they y-axis (Pitch axis)
%psi is the angle with repsect to they z-axis (Pitch axis)

% g.marechal 2021 SUMOS campain (from SBG system usersheet p.31)

%   Rot_x=/1    0      0        0\
%         |0  cos(phi) -sin(phi) 0|
%         |0  sin(phi) cos(phi)  0|
%         \0     0      0       1/        ->[-pi,pi]

%   Rot_y=/cos(theta)   0      sin(theta)  0\
%        |0            1         0         0|
%        |-sin(theta)  0      cos(theta)   0|
%         \0            0           0      1/ ->[-pi/2,pi/2]

%   Rot_z=/cos(psi)   -sin(psi)       0       0\
%         |sin(psi)   cos(psi)        0       0 |   
%         |0          0               1       0 |
%         \0          0               0       1/->[-pi,pi]

Rot_x=[1,0,0,0;...
       0,cos(phi), -sin(phi),0;...
        0,sin(phi),cos(phi),0;...
        0,0,0,1];

Rot_y=[cos(theta),0,sin(theta),0;...
                        0,1, 0,0;...
        -sin(theta),0,cos(theta),0;...
        0,0,0,1];


Rot_z=[cos(psi),-sin(psi),0,0;...
    sin(psi),cos(psi), 0,0;...
        0,0,1,0; ...
        0,0,0,1];

end

