function Rotates(varargin)
%
% This function computes points coordinates in world reference coordinate 
% system using mean plane parameters from Compute_Mean_Plane.m.
%
% USAGE : Rotates([opt,]'NNNNNN'[,MMMMMM])
%
% OPTION : -f force rotation even if 3D/Rot_NNNNN.mat  
%             already exist.
% 
% => Input  : 3D/Tri_NNNNNN.mat and 3D/Mean_Plane.mat
% => Output : 3D/Rot_NNNNNN.mat
%
% sub-functions used :
%   - read_arguments.m
%   - apply_rot_trans.m
%

% function name
func=dbstack;

fprintf('################################################################\n')
fprintf('%s: Function starts \n', func(1).name)
fprintf('################################################################\n')

% readlink
dir_3D = readlink('3D');

% read arguments
[Nstr,StrNums,FORCE] = read_arguments(varargin{:});

% mean plane file name (needed)
MeanPlaneFile = sprintf('%s/Mean_Plane.mat',dir_3D);
fprintf('Load %s ... ', MeanPlaneFile)
load(MeanPlaneFile,'R_R','T_R')
R_R = double(R_R);
T_R = double(T_R);
fprintf('DONE \n')

% initialize error parameters
NERR = 0;
Failed_Process = struct([]);

% loop over strlist
for istr =  1:Nstr
  
    %clean
    clear xyzL xyz
    
    % Get StrNum
    StrNum = StrNums{istr};
    fprintf('Processing for %s :\n', StrNum)

    try

        % file names
        TriFile = sprintf('%s/Tri_%s.mat', dir_3D, StrNum);
        RotFile = sprintf('%s/Rot_%s.mat', dir_3D, StrNum);
        
        % check existance
        if ~exist(TriFile,'file')
            error('%s: %s don''t found !\n', func(1).name, TriFile)
        end
        
        % already done
        if exist(RotFile,'file')  && ~FORCE
            fprintf('==> ALREADY DONE !\n');
            continue
        end
        
        % Load correlated point coordinates
        fprintf('Load %s ... ', TriFile);
        load(TriFile,'pixL','pixR','xyzR')
        pixL = double(pixL);
        pixR = double(pixR);
        xyzR = double(xyzR);
        fprintf('DONE\n')
        
        % rotate
        fprintf('Process Rotation ...')
        [xyz] = apply_rot_trans(xyzR,R_R,T_R);

        fprintf('DONE\n')
        
        
        % get min et max boarder of reconstruction
        fprintf('Get limits ... ')
        xmin = min(xyz(1,:));
        xmax = max(xyz(1,:));
        ymin = min(xyz(2,:));
        ymax = max(xyz(2,:));
        zmin = min(xyz(3,:));
        zmax = max(xyz(3,:));
        fprintf('DONE \n')
        fprintf('=> xmin = %f \n', xmin)
        fprintf('=> xmax = %f \n', xmax)
        fprintf('=> ymin = %f \n', ymin)
        fprintf('=> ymax = %f \n', ymax)
        fprintf('=> zmin = %f \n', zmin)
        fprintf('=> zmax = %f \n', zmax)
        
        % save result file
        fprintf('Save results in %s ... ', RotFile)
        pixL = single(pixL);
        pixR = single(pixR);
        xyz  = single(xyz);
        xmin = single(xmin);
        xmax = single(xmax);
        ymin = single(ymin);
        ymax = single(ymax);
        zmin = single(zmin);
        zmax = single(zmax);
        Comment = sprintf('Created by %s (%s)',func(1).name,date);
        save(RotFile,'xyz','pixL','pixR',...
            'xmin','xmax','ymin','ymax','zmin','zmax','Comment')
        fprintf('DONE\n')
        
    catch me

        NERR = NERR+1;
        
        Failed_Process(NERR).StrNum = StrNum;
        Failed_Process(NERR).Report   = getReport(me);
        
        fprintf('PROCESSING FAILS !!!\n==> SKIPPED\n')

    end
    

end

if NERR > 0
    
    fprintf('##########################   WARNING  ##########################\n')
    fprintf('%s: Processing fails for %d couple(s) of images.\n', ...
        func(1).name, NERR);

    % list error
    fprintf('List of skipped process : \n');
    for ierr = 1:NERR
        fprintf('%s \n',Failed_Process(ierr).StrNum);
    end
    fprintf('################################################################\n')

    % error output
    ERR = [];
    for ierr = 1:NERR
        ERR = sprintf('%s ==== Processing of %s ==== \n %s \n', ERR, ...
            Failed_Process(ierr).StrNum, Failed_Process(ierr).Report);
    end
    error(ERR)
    
else
    
    fprintf('################################################################\n')
    fprintf('%s: Program ends without errors\n', func(1).name)
    fprintf('################################################################\n')
    
end


end
