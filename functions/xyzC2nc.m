function xyzC2nc(iidx)
%
% This function computes points coordinates in world reference coordinate 
% system using mean plane parameters from plane.txt
%
% USAGE : xyzC2nc('000001')
%
% OPTION : -f force rotation even if 3D/Surface_NNNNN.nc  
%             already exist. (not implemented)
% 
% => Input  : datadir = ../run_2016-10-17_15h06m30.189sZ/output/
%             outdir  = ../run_2016-10-17_15h06m30.189sZ/3D/
%             calibdir='../Calib/'
%             idx     = 0, 1, 2, ... 
%
% => Output : 3D/Surface_NNNNNN.nc
%
% sub-functions used :
%   - load_camera_mesh
%   - Define_limits_for_grid
%   -
%

% function name
func=dbstack;

fprintf('################################################################\n')
fprintf('%s: Function starts \n', func(1).name)
fprintf('################################################################\n')

% directory definitions 
%work_dir=pwd;
work_dir='E:/STEREO_DATA_DAY_2021_02_19/TIF_20210219_082101';
datadir=[work_dir,'/output/'];
outdir=[work_dir,'/3D/'];
%calibdir='/home/cercache/project/stereo/DYNATREZ_2016/raw/Calib/';
% idx = 1;
idx=str2num(iidx);

% define plane file
planefile='plane.txt';

%% Get scale
%assert( ~(~exist(['../Calib/Calib_Results_stereo.mat'],'file')), '../Calib/Calib_Results_stereo.mat file do not exist.')
%T0=load(['../Calib/Calib_Results_stereo.mat'],'T');
%scale = T0.T(1)/1000; % T0.T correspond to the base line and /1000 for convert to meters
scale=4219e-3;
%LOAD_CAMERA_MESH_AND_ALIGN_PLANE

workdir = sprintf( '%s%06d_wd/', datadir, idx);
cd(workdir);

try
    mesh_cam = load_camera_mesh();
    n_pts = size(mesh_cam,2);
    plane = importdata(planefile);
    assert( size(plane,1)==4, 'invalid plane file');
    

    
    
catch me
    mesh_cam = NaN(3,3); plane = NaN(4,1);
    n_pts = size(mesh_cam,2);
    R=eye(3);T=zeros(3,1);

    fprintf(sprintf('Could not load mesh_cam.xyzC it will create empty 3D/Surface_%06.0f.nc \n',idx))    
end




%% Compute RT from plane 

a=plane(1);b=plane(2);c=plane(3);d=plane(4);
q = (1-c)/(a*a + b*b);
R=eye(3);T=zeros(3,1);
R(1,1) = 1-a*a*q;
R(1,2) = -a*b*q;
R(1,3) = -a;
R(2,1) = -a*b*q;
R(2,2) = 1-b*b*q;
R(2,3) = -b;
R(3,1) = a;
R(3,2) = b;
R(3,3) = c;
T(1)=0;
T(2)=0;
T(3)=d;


    
%% Rotate, translate
mesh=R*mesh_cam + repmat(T,1,n_pts);

mesh=mesh';
% apply scale
mesh = mesh*scale;
% Invert z axis
mesh(:,3)=mesh(:,3)*-1.0;

%assert( size(mesh,1)>3E3, sprintf('Mesh %s has less than 3E3 points',workdir));
    
%% load correspondent pixel

%% Define grid surface
cd(work_dir)
Grid_file=['config/Grid.mat'];
if ~exist(Grid_file)
    xyz=mesh';
    save([sprintf('3D/Rot_%06d.mat',idx)],'xyz')
    Define_limits_for_grid
    system(['mv 3D/Grid.mat ',Grid_file]);
    system(['rm ',sprintf('3D/Rot_%06d.mat',idx)]);
end

%% Grid surface and save 3D/Surface_%.nc
system( ['mkdir -p ',outdir] );

P1 = importdata([workdir,'P1cam.txt']);
im = imread([workdir,'undistorted/00000001.png']);
force=1;
grid_surface(mesh',P1,Grid_file,outdir,idx,force)

end

