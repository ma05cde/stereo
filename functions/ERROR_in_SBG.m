clc;clear;close all;clear path

set(0,'defaultAxesFontName', 'Serif')
set(0,'defaultTextFontN', 'Serif')
set(0,'DefaultAxesFontSize',16)
set(0,'DefaultTextFontSize',16)
set(0,'defaultfigurecolor',[1 1 1]*1)
scrsz = get(0,'ScreenSize');
%%
fonctions_path='F:\MATLAB\functions'
%fonctions_path='/media/gmarecha/Dell USB/MATLAB/functions'

cd (fonctions_path)

%% LOAD in struct SBG files (saved somewhere in Datarmor or in your computer)


myDir = 'C:\Users\stereo\Desktop\SBG_data';
myFiles = dir(fullfile(myDir,'*.dat')); %gets all .dat files in struct

%% IMU
% Read IMU data

%IMU = sbg_load_fast(fname_sbg,save_mat,fname_out);
indir_SBG='F:\SUMOS';
%indir_SBG='/media/gmarecha/Dell USB/SUMOS/'



infile_SBG='sbg_20210304_060000.dat'; %MARC shows low wave height in the Gulf of Biscay the 03.04.21

S=sbg_load_fast(fullfile(indir_SBG,infile_SBG),0);%P.Sutherland function

S1 = sbg_imu_time_fields(S);

%%%%%%%%%%
%----GPS
%%%%%%%%%%
ALT_GPS=S1.GPS1_POS.ALT;
ALT_EKF=S1.EKF_NAV.ALTITUDE;

%%%%%%%%%%
%----timestamp (loop over 2^32 time-measurements for raw struct.)
%%%%%%%%%%
ts_EKF=S.EKF_NAV.TIME_STAMP; %25Hz acquisition
ts_GPS=S.GPS1_POS.TIME_STAMP; %5Hz acquisition

    %%%%
    %PLOT timestamp
    %%%%
    figure(2),clf
    plot(ts_EKF,'b')
    hold
    plot(ts_GPS,'r')
    xlabel('NUMBER OF ITEM','fontsize',22)
    ylabel('TIMESTAMP [ns]')
    legend('EKF timestamp','GPS timestamp')
    text(time_GPS_vec(end),13,'Baseline elevation')

    
    
    
%%%%%%%%%%
%----time (clean time data using sbg_imu_time_fields.m)
%%%%%%%%%%
time_EKF=S1.EKF_NAV.time; %25Hz acquisition
time_GPS=S1.GPS1_POS.time; %5Hz acquisition
%vectors
time_EKF_vec=time_EKF; %In sec
time_GPS_vec=time_GPS;
    %%%%
    %PLOT
    %%%%
    figure(3),clf
    plot(time_EKF_vec)
    hold on
    plot(time_GPS_vec)
    xlabel('NUMBER OF ITEM','fontsize',22)
    ylabel('Time')
    

%%%%%%%%%%
%----Plot
%%%%%%%%%%
figure(1),clf
plot(time_EKF_vec,ALT_EKF,'bx-')
hold on
plot(time_GPS_vec,ALT_GPS,'rx-')
plot(time_GPS_vec,12.8,'k')
xlabel('TIME')
ylabel('SBG Elevations')
title('MARC HINDCAST H_{s}~1m, \eta _{tide}~-1.5m')
datetick('x','keepticks','keeplimits') %datetime format
legend('EKF Alt.','GPS Alt.')
text(time_GPS_vec(end),13,'Baseline elevation')
%%
%SWITCH TIMESTAMP

h_start = datenum([min([S.UTC_TIME.YEAR, S.UTC_TIME.MONTH, S.UTC_TIME.DAY, S.UTC_TIME.HOUR]) 0 0]);
h_end= datenum([max([S.UTC_TIME.YEAR, S.UTC_TIME.MONTH, S.UTC_TIME.DAY, S.UTC_TIME.HOUR]) 0 0]);

time_sec = datenum([S.UTC_TIME.YEAR, S.UTC_TIME.MONTH, S.UTC_TIME.DAY, S.UTC_TIME.HOUR, S.UTC_TIME.MIN, S.UTC_TIME.SEC]);
% time in seconds since the start of the hour
ts=time_GPS/1e6/86400+h_start;
%t_vec=linspace(min(t),max(t),25*length(t));%5 data per second


%plot in function of date 
plot(ts,ALT_GPS),
%% Work on ALL the SBG files

indir_SBG=myDir;

for i_file=1:length(myFiles)
    infile_SBG=myFiles(i_file).name;
    try
    S=sbg_load_fast(fullfile(indir_SBG,infile_SBG),0);%P.Sutherland function
    S1 = sbg_imu_time_fields(S);

    %%%%%%%%%%
    %----GPS
    %%%%%%%%%%
    ALT_GPS=S1.GPS1_POS.ALT;
    ALT_EKF=S1.EKF_NAV.ALTITUDE;

    
    %%%%%%%%%%
    %----time (clean time data using sbg_imu_time_fields.m)
    %%%%%%%%%%
    time_EKF=S1.EKF_NAV.time; %25Hz acquisition
    time_GPS=S1.GPS1_POS.time; %5Hz acquisition
    %vectors
    time_EKF_vec=time_EKF; %In sec
    time_GPS_vec=time_GPS;
    vec_baseline=linspace(12.8,12.8,length(time_GPS_vec));

    h=figure(1),clf
    l1=plot(time_EKF_vec,ALT_EKF,'bx-')
    hold on
    l2=plot(time_GPS_vec,ALT_GPS,'rx-')
    xlabel('TIME')
    ylabel('SBG Elevations')
    titlex=sprintf('%s',infile_SBG);
    title(titlex)
    datetick('x','keepticks','keeplimits') %datetime format
    text(time_GPS_vec(end),13,'Baseline elevation')
    plot(time_GPS_vec,vec_baseline,'-k')
    legend([l1,l2],'EKF Alt.','GPS Alt.')


    fnm = sprintf('C:/Users/stereo/Desktop/SBG_data/figures_ALT_SBG/SBG_%s.jpg',infile_SBG);
    saveas(h,fnm)
    catch
        disp('Structure not read')
    end
end




