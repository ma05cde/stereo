function [point_cloud_rotate] = CAM_to_WORLD_coord(points_cloud,R_world,R_obj,T_obj)
%Rotate point cloud from the camera frame into the world coordinate
%Ximu=Xcam*Rimu/cam+t_cam/imu
%Xworld=Ximu*DCM+t_cam/imu

%g.marechal march 2021 SUMOS campain
rot_elevations=R_obj*points_cloud;
shift_elevations=rot_elevations+T_obj';
point_cloud_rotate=R_world*shift_elevations;
%Then translation in meter due to boat trajectory. So not a superposition
%of frame in one point but superposition of frame along a track
end

