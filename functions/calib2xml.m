function calib2xml(calib_file)
% Create from Calib_Results_stereo.mat file 
% distortion_00.xml
% distortion_01.xml
% intrinsics_00.xml
% intrinsics_01.xml
% ext_R.xml
% ext_T.xml
%

%% Load file

load(calib_file)


%% Left
disp(' Creating distortion_00.xml')
fid = fopen('distortion_00.xml', 'w' );
fprintf(fid, '<?xml version="1.0"?> \n');
fprintf(fid, '<opencv_storage>\n');
fprintf(fid, '<intrinsics_penne type_id="opencv-matrix">\n'); 
fprintf(fid, ['  <rows>',num2str(size(kc_left,1)),'</rows> \n']); 
fprintf(fid, ['  <cols>',num2str(size(kc_left,2)),'</cols> \n']); 
fprintf(fid, '  <dt>d</dt> \n'); 
fprintf(fid, '  <data> \n'); 
for ii=1:size(kc_left,1)
    fprintf(fid, '    %0.15e \n',kc_left(ii) );
end
fprintf(fid, '  </data></intrinsics_penne> \n'); 
fprintf(fid, '</opencv_storage> \n'); 
fclose(fid);

disp(' Creating intrinsics_00.xml')
fid = fopen('intrinsics_00.xml', 'w' );
fprintf(fid, '<?xml version="1.0"?> \n');
fprintf(fid, '<opencv_storage>\n');
fprintf(fid, '<intrinsics_penne type_id="opencv-matrix">\n'); 
fprintf(fid, ['  <rows>',num2str(size(KK_left,1)),'</rows> \n']); 
fprintf(fid, ['  <cols>',num2str(size(KK_left,2)),'</cols> \n']); 
fprintf(fid, '  <dt>d</dt> \n'); 
fprintf(fid, '  <data> \n'); 
for ii=1:size(KK_left,1)
    fprintf(fid, '    %0.15e    %0.15e    %0.15e\n',KK_left(ii,:) );
end
fprintf(fid, '  </data></intrinsics_penne> \n'); 
fprintf(fid, '</opencv_storage> \n'); 
fclose(fid);

%% Right
disp(' Creating distortion_01.xml')
fid = fopen('distortion_01.xml', 'w' );
fprintf(fid, '<?xml version="1.0"?> \n');
fprintf(fid, '<opencv_storage>\n');
fprintf(fid, '<intrinsics_penne type_id="opencv-matrix">\n'); 
fprintf(fid, ['  <rows>',num2str(size(kc_right,1)),'</rows> \n']); 
fprintf(fid, ['  <cols>',num2str(size(kc_right,2)),'</cols> \n']); 
fprintf(fid, '  <dt>d</dt> \n'); 
fprintf(fid, '  <data> \n'); 
for ii=1:size(kc_right,1)
    fprintf(fid, '    %0.15e \n',kc_right(ii) );
end
fprintf(fid, '  </data></intrinsics_penne> \n'); 
fprintf(fid, '</opencv_storage> \n'); 
fclose(fid);

disp(' Creating intrinsics_01.xml')
fid = fopen('intrinsics_01.xml', 'w' );
fprintf(fid, '<?xml version="1.0"?> \n');
fprintf(fid, '<opencv_storage>\n');
fprintf(fid, '<intrinsics_penne type_id="opencv-matrix">\n'); 
fprintf(fid, ['  <rows>',num2str(size(KK_right,1)),'</rows> \n']); 
fprintf(fid, ['  <cols>',num2str(size(KK_right,2)),'</cols> \n']); 
fprintf(fid, '  <dt>d</dt> \n'); 
fprintf(fid, '  <data> \n'); 
for ii=1:size(KK_right,1)
    fprintf(fid, '    %0.15e    %0.15e    %0.15e \n',KK_right(ii,:) );
end
fprintf(fid, '  </data></intrinsics_penne> \n'); 
fprintf(fid, '</opencv_storage> \n'); 
fclose(fid);

%% EXTRINSIC
disp(' Creating ext_R_calib.xml')
fid = fopen('ext_R_calib.xml', 'w' );
fprintf(fid, '<?xml version="1.0"?> \n');
fprintf(fid, '<opencv_storage> \n');
fprintf(fid, '<ext_R type_id="opencv-matrix"> \n');
fprintf(fid, ['  <rows>3</rows> \n']);
fprintf(fid, ['  <cols>3</cols> \n']);
fprintf(fid, '  <dt>d</dt> \n');
fprintf(fid, '  <data> \n');
fprintf(fid, '    %1.15f    %1.15f    %1.15f \n',R(1,:) );
fprintf(fid, '    %1.15f    %1.15f    %1.15f \n',R(2,:) );
fprintf(fid, '    %1.15f    %1.15f    %1.15f',R(3,:) );
fprintf(fid, '</data></ext_R> \n');
fprintf(fid, '</opencv_storage> \n');
fclose(fid);


disp(' Creating ext_T_calib.xml')
fid = fopen('ext_T_calib.xml', 'w' );
fprintf(fid, '<?xml version="1.0"?> \n');
fprintf(fid, '<opencv_storage> \n');
fprintf(fid, '<ext_T type_id="opencv-matrix"> \n');
fprintf(fid, ['  <rows>3</rows> \n']);
fprintf(fid, ['  <cols>1</cols> \n']);
fprintf(fid, '  <dt>d</dt> \n');
fprintf(fid, '  <data> \n');
fprintf(fid, '    %0.15f',T(1) );
fprintf(fid, '   %0.15f  \n',T(2) );
fprintf(fid, '    %0.15f',T(3) );
fprintf(fid, '</data></ext_T> \n');
fprintf(fid, '</opencv_storage> \n');
fclose(fid);


%%
disp('Done')
